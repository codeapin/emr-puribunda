using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.IO;
using ClosedXML.Excel;

namespace EmrPuriBunda.Controllers
{
    [Authorize]
    public class LaporanKelengkapanPengisianAssesmenAwalMedisController : Controller
    {
        private IConfiguration Config { get; set; }
        public LaporanKelengkapanPengisianAssesmenAwalMedisController(IConfiguration configuration) => Config = configuration;

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_LaporanKelengkapanPengisianAssesmenAwalMedis(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<LaporanKelengkapanPengisianAssesmenAwalMedis_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "NoReg",
                        "NRM",
                        "JenisPelayanan",
                        "NamaDOkter",
                        "StatusLengkap",
                        "S",
                        "O",
                        "A",
                        "P"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Mulai":
                                parameters.Add(new SqlParameter($"@FromDate", $"{x.Value}"));
                                break;
                            case "Akhir":
                                parameters.Add(new SqlParameter($"@ToDate", $"{x.Value}"));
                                break;
                            case "GroupSection":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@GroupSection", $"{x.Value}"));
                                wheres.Add($"JenisPelayanan = @GroupSection");
                                break;
                            case "NRM":
                                parameters.Add(new SqlParameter($"@NRM", $"%{x.Value}%"));
                                wheres.Add($"NRM LIKE @NRM");
                                break;
                            case "NamaDokter":
                                parameters.Add(new SqlParameter($"@NamaDokter", $"%{x.Value}%"));
                                wheres.Add($"NamaDokter LIKE @NamaDokter");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            FT_EMR_KelengkapanPengisianAssesmenAwalMedis(@FromDate, @ToDate)
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<LaporanKelengkapanPengisianAssesmenAwalMedis_ListModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpGet]
        [ActionName("ReportExcel")]
        public async Task<ActionResult> ReportExcelAsync(DateTime dariTanggal, DateTime sampaiTanggal, string groupSection)
        {
            var dt = new DataTable("Laporan");
            dt.Columns.AddRange(
                new DataColumn[]
                {
                    new DataColumn("NoReg"),
                    new DataColumn("NRM"),
                    new DataColumn("JenisPelayanan"),
                    new DataColumn("NamaDOkter"),
                    new DataColumn("StatusLengkap"),
                    new DataColumn("S"),
                    new DataColumn("O"),
                    new DataColumn("A"),
                    new DataColumn("P"),
                });

            List<LaporanKelengkapanPengisianAssesmenAwalMedis_ListModel> datas;

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                var wheres = new List<string>();
                var parameters = new List<SqlParameter>
                {
                    new SqlParameter("@FromDate", dariTanggal.ToString("yyyy-MM-dd")),
                    new SqlParameter("@ToDate", sampaiTanggal.ToString("yyyy-MM-dd")),
                };

                if (!string.IsNullOrEmpty(groupSection))
                {
                    parameters.Add(new SqlParameter($"@GroupSection", groupSection));
                    wheres.Add($"JenisPelayanan = @GroupSection");
                }

                var query = $@"SELECT
                            *
                        FROM
                            FT_EMR_KelengkapanPengisianAssesmenAwalMedis(@FromDate, @ToDate)
                        {s.ToWhere(wheres)}";

                datas = await s.GetDatas<LaporanKelengkapanPengisianAssesmenAwalMedis_ListModel>(query, parameters);
            }

            foreach (var item in datas)
            {
                dt.Rows.Add(
                    item.NoReg,
                    item.NRM,
                    item.SectionName,
                    item.NamaDOkter,
                    item.StatusLengkap,
                    item.S,
                    item.O,
                    item.A,
                    item.P
                );
            }

            using var wb = new XLWorkbook();

            var ws = wb.Worksheets.Add("Laporan");

            ws.Cell(1, 1).Value = "Periode";
            ws.Cell(1, 1).AsRange().AddToNamed("Titles");

            ws.Cell(2, 1).Value = "Dari Tanggal";
            ws.Cell(2, 2).Value = dariTanggal;

            ws.Cell(3, 1).Value = "Sampai Tanggal";
            ws.Cell(3, 2).Value = sampaiTanggal;

            ws.Cell(5, 1).Value = "Group Section";
            ws.Cell(5, 1).AsRange().AddToNamed("Titles");
            ws.Cell(5, 2).Value = string.IsNullOrEmpty(groupSection) ? "All" : groupSection;

            ws.Cell(7, 1).Value = "Jml Pasien";
            ws.Cell(7, 1).AsRange().AddToNamed("Titles");
            ws.Cell(7, 2).Value = datas.Count;

            var colNama = 0;
            var colTable = 9;

            ws.Cell(colTable, ++colNama).Value = "NoReg";
            ws.Cell(colTable, ++colNama).Value = "NRM";
            ws.Cell(colTable, ++colNama).Value = "Jenis Pelayanan";
            ws.Cell(colTable, ++colNama).Value = "Nama Dokter";
            ws.Cell(colTable, ++colNama).Value = "Status Lengkap";
            ws.Cell(colTable, ++colNama).Value = "S";
            ws.Cell(colTable, ++colNama).Value = "O";
            ws.Cell(colTable, ++colNama).Value = "A";
            ws.Cell(colTable, ++colNama).Value = "P";

            ws.Range(colTable, 1, colTable, 9).AddToNamed("Titles");

            ws.Cell(++colTable, 1).InsertData(dt);

            var titlesStyle = wb.Style;
            titlesStyle.Font.Bold = true;
            titlesStyle.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            titlesStyle.Fill.BackgroundColor = XLColor.AshGrey;

            // Format all titles in one shot
            wb.NamedRanges.NamedRange("Titles").Ranges.Style = titlesStyle;

            ws.Columns().AdjustToContents();

            using var stream = new MemoryStream();
            wb.SaveAs(stream);

            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "LaporanKelengkapanPengisianAssesmenAwalMedis.xlsx");
        }
    }
}