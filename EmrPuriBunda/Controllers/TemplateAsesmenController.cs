using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using EmrPuriBunda.Constant;

namespace EmrPuriBunda.Controllers
{
    public class TemplateAsesmenController : Controller
    {
        private IConfiguration Config { get; set; }
        public TemplateAsesmenController(IConfiguration configuration) => Config = configuration;

        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Policy = PolicyConstant.SetupMaster)]
        [HttpPost]
        public async Task<JsonResult> Datas_TemplateAsesmen(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<TemplateAsesmen_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "Kode",
                        "Judul",
                        "TempData",
                        "SpesialisasiID_Text",
                        "JenisID_Text",
                        "DokterID_Text",
                        "SectionID_Text"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));
                                wheres.Add($"Kode LIKE @Filter OR Judul LIKE @Filter");
                                break;
                            case "Jenis":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter("@Jenis", $"%{x.Value}%"));
                                wheres.Add($"JenisID_Text LIKE @Jenis");
                                break;
                            case "Section":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter("@SectionID", x.Value));
                                wheres.Add($"SectionID = @SectionID");
                                break;
                            case "Spesialisasi":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter("@SpesialisasiID", x.Value));
                                wheres.Add($"SpesialisasiID = @SpesialisasiID");
                                break;
                            case "Dokter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter("@DokterID", x.Value));
                                wheres.Add($"DokterID = @DokterID");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_mTemplateAsesmen
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<TemplateAsesmen_ListModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [Authorize(Policy = PolicyConstant.SetupMaster)]        [HttpGet]
        [ActionName("TemplateAsesmen")]
        public async Task<JsonResult> Data_TemplateAsesmen(int Id)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                TemplateAsesmen_SetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_mTemplateAsesmen
                        WHERE
                            Id=@Id
                    ";

                    data = await s.GetData<TemplateAsesmen_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", Id),
                    });

                    query = $@"						SELECT							*						FROM							Vw_mTemplateAsesmen_DetailJenis						WHERE							Id_mTemplateAsesmen=@Id_mTemplateAsesmen					";
                    data.TableJenis = await s.GetDatas<TemplateAsesmen_TableJenis_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id_mTemplateAsesmen", Id)
                    });
                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [Authorize(Policy = PolicyConstant.SetupMaster)]
        [HttpPost]
        [ActionName("TemplateAsesmen")]
        public async Task<JsonResult> Post_TemplateAsesmen(TemplateAsesmen_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    if (model.TableJenis == null || model.TableJenis.Count == 0) throw new Exception("Jenis Template Asesmen tidak boleh kosong");

                    s.OpenTransaction();

                    model.Id = await s.Insert(model, true);
                    if (model.TableJenis == null) model.TableJenis = new List<TemplateAsesmen_TableJenis_SetupModel>();
                    foreach (var x in model.TableJenis)
                    {
                        x.Id_mTemplateAsesmen = model.Id;
                        await s.Insert(x);
                    }

                    if (!string.IsNullOrEmpty(model.Id_AspNetRoles))
                    {
                        var tableRoles = new TemplateAsesmen_TableRoles_SetupModel() 
                        {
                            Id_mTemplateAsesmen = model.Id,
                            Id_AspNetRoles = model.Id_AspNetRoles
                        };

                        await s.Insert(tableRoles);
                    }

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        [Authorize(Policy = PolicyConstant.SetupMaster)]
        [HttpPut]
        [ActionName("TemplateAsesmen")]
        public async Task<JsonResult> Put_TemplateAsesmen(int Id, TemplateAsesmen_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    if (model.TableJenis == null || model.TableJenis.Count == 0) throw new Exception("Jenis Template Asesmen tidak boleh kosong");

                    var oldModel = new TemplateAsesmen_SetupModel();
                    var query = "";
                    query = $@"						SELECT							*						FROM							mTemplateAsesmen_DetailJenis						WHERE							Id_mTemplateAsesmen=@Id_mTemplateAsesmen					";
                    oldModel.TableJenis = await s.GetDatas<TemplateAsesmen_TableJenis_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id_mTemplateAsesmen", Id)
                    });

                    s.OpenTransaction();
                    model.Id = Id;
                    await s.Update(model);

                    model.TableJenis ??= new List<TemplateAsesmen_TableJenis_SetupModel>();

                    foreach (var x in oldModel.TableJenis)
                    {
                        var oldModelNotInNewModel = model.TableJenis.FirstOrDefault(y =>
                            y.Id_mJenisTemplateAsesmen == x.Id_mJenisTemplateAsesmen) == null;

                        if (oldModelNotInNewModel)
                        {
                            await s.Delete(x);
                        }
                    }
                    foreach (var x in model.TableJenis)
                    {
                        x.Id_mTemplateAsesmen = model.Id;

                        if (oldModel.TableJenis.FirstOrDefault(y =>
                            y.Id_mJenisTemplateAsesmen == x.Id_mJenisTemplateAsesmen
                        ) == null)
                        {
                            await s.Insert(x);
                        }
                    }

                    await s.ExecuteNonQuery("DELETE FROM mTemplateAsesmen_DetailRoles WHERE Id_mTemplateAsesmen = @Id", new List<SqlParameter>()
                    {
                        new SqlParameter("@Id", model.Id)
                    });


                    if (!string.IsNullOrEmpty(model.Id_AspNetRoles))
                    {
                        var tableRoles = new TemplateAsesmen_TableRoles_SetupModel()
                        {
                            Id_mTemplateAsesmen = model.Id,
                            Id_AspNetRoles = model.Id_AspNetRoles
                        };

                        await s.Insert(tableRoles);
                    }

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        [Authorize(Policy = PolicyConstant.SetupMaster)]
        [HttpDelete]
        [ActionName("TemplateAsesmen")]
        public async Task<JsonResult> Delete_TemplateAsesmen(int Id)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    var model = new TemplateAsesmen_SetupModel();
                    var query = "";
                    query = $@"						SELECT							*						FROM							mTemplateAsesmen_DetailJenis						WHERE							Id_mTemplateAsesmen=@Id_mTemplateAsesmen					";
                    model.TableJenis = await s.GetDatas<TemplateAsesmen_TableJenis_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id_mTemplateAsesmen", Id)
                    });

                    s.OpenTransaction();

                    foreach (var x in model.TableJenis) await s.Delete(x);
                    model.Id = Id;
                    await s.Delete(model);
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }        [Authorize]        [HttpPost]
        public async Task<JsonResult> mJenisTemplateAsesmen(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("JenisTemplateAsesmen LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            Id AS Value,
                            JenisTemplateAsesmen AS Text
                        FROM
                            mJenisTemplateAsesmen
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [Authorize]        [HttpPost]
        public async Task<JsonResult> Select_JenisTemplateAsesmen(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("JenisTemplateAsesmen LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            JenisTemplateAsesmen AS Value,
                            JenisTemplateAsesmen AS Text
                        FROM
                            mJenisTemplateAsesmen
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [Authorize]        [HttpPost]
        public async Task<JsonResult> mSpesialisasi(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("SpesialisName LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            SpesialisID AS Value
                            , SpesialisName AS Text
                        FROM
                            mSpesialisasi
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [Authorize]        [HttpPost]
        public async Task<JsonResult> mDokter(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("NamaDOkter LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            DokterID AS Value
                            , NamaDOkter AS Text
                        FROM
                            mDokter
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<JsonResult> Select_Roles(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("UserIdentity")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Name LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            Id AS Value
                            , Name AS Text
                        FROM
                            AspNetRoles
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<JsonResult> Section(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("SectionName LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            SectionID AS Value
                            , SectionName AS Text
                        FROM
                            SIMmSection
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<JsonResult> Select_TemplateAsesmen(
            int pageSize, 
            int pageIndex, 
            string filter, 
            int? jenisID,
            string sectionID,
            string dokterID
        )
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<TemplateAsesmen_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>() 
                    {
                        new SqlParameter("@JenisID", (object) jenisID ?? DBNull.Value ),
                        new SqlParameter("@SectionID", (object) sectionID ?? DBNull.Value ),
                        new SqlParameter("@SpesialisasiID", (object) null ?? DBNull.Value ),
                        new SqlParameter("@DokterID", (object) dokterID ?? DBNull.Value ),
                        new SqlParameter("@UserId", (object) userid ?? DBNull.Value ),
                    };

                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Kode LIKE @Text OR Judul LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            get_list_mTemplateAsesmen(
                                @JenisID,
                                @SectionID,
                                @SpesialisasiID,
                                @DokterID,
                                @UserId
                            )
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<TemplateAsesmen_ListModel>(query, "Kode", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<JsonResult> LookUp_TableJenis(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<TemplateAsesmen_TableJenis_LookUpModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "Id",
                        "JenisTemplateAsesmen"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));
                                wheres.Add($"JenisTemplateAsesmen LIKE @Filter");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            mJenisTemplateAsesmen
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<TemplateAsesmen_TableJenis_LookUpModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}