using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace EmrPuriBunda.Controllers
{
    // RoleX
    public class RiwayatAlergiPasienController : BaseController
    {
        public RiwayatAlergiPasienController(IConfiguration config) : base(config)
        {
        }        [HttpGet]
        [ActionName("RiwayatAlergiPasien")]
        public async Task<JsonResult> Data_RiwayatAlergiPasien(string nrm)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                RiwayatAlergiGabung data = new();
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"SELECT dbo.trRiwayatAlergiPasien_getAlergi(@nrm) AS RiwayatAlergi";

                    data = await s.GetData<RiwayatAlergiGabung>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@nrm", nrm),
                    });

                    
                }
                if (data == null) data = new RiwayatAlergiGabung();
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}