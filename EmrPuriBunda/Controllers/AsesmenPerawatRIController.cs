using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using EmrPuriBunda.Constant;
using EmrPuriBunda.Mahas.Helpers;

namespace EmrPuriBunda.Controllers
{
    [Authorize(Policy = PolicyConstant.SadurAsesmen)]
    public class AsesmenPerawatRIController : BaseController
    {
        public AsesmenPerawatRIController(IConfiguration config) : base(config)
        {
        }

        [HttpGet]
        [ActionName("ActionAsesmenPerawatRI")]
        public async Task<JsonResult> Data_AsesmenPerawatRI(string NoReg)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                AsesmenPerawatRI_SetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_AsesmenPerawatRI
                        WHERE
                            NoReg=@NoReg
                    ";

                    data = await s.GetData<AsesmenPerawatRI_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoReg", NoReg),
                    });

                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        // RoleX
        [HttpPost]
        [ActionName("ActionAsesmenPerawatRI")]
        public async Task<JsonResult> Post_AsesmenPerawatRI(AsesmenPerawatRI_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var section = Request.GetSection();

            if (!ModelState.IsValid)
            {
                return InvalidModelResult();
            }

            try
            {
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    model.UserId = userid;
                    model.SectionId = section.SectionID;

                    s.OpenTransaction();
                    await s.Insert(model);

                    await s.ExecuteNonQuery("EXEC Sp_InsertCpptAsesmenPerawatRI @NoReg", new List<SqlParameter>()
                    {
                        new SqlParameter("@NoReg", model.NoReg)
                    });

                    s.Transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpPut]
        [ActionName("ActionAsesmenPerawatRI")]
        public async Task<JsonResult> Put_AsesmenPerawatRI(string NoReg, AsesmenPerawatRI_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var section = Request.GetSection();

            if (!ModelState.IsValid)
            {
                return InvalidModelResult();
            }

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            AsesmenPerawatRI
                        WHERE
                            NoReg=@NoReg
                    ";

                    var oldModel = await s.GetData<AsesmenPerawatRI_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoReg", NoReg),
                    });

                    if (oldModel.UserId.Equals(userid, StringComparison.OrdinalIgnoreCase) == false) throw new Exception("Login salah. Hanya user yang menyimpan yang boleh mengubah.");

                    //model.WaktuPelayanan = oldModel.WaktuPelayanan;
                    model.UserId = userid;
                    model.SectionId = section.SectionID;
                    model.NoReg = NoReg;

                    s.OpenTransaction();

                    await s.Update(model);

                    await s.ExecuteNonQuery("EXEC Sp_UpdateCpptAsesmenPerawatRI @NoReg", new List<SqlParameter>()
                    {
                        new SqlParameter("@NoReg", model.NoReg)
                    });

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }        [HttpPost]
        public async Task<JsonResult> Vw_Select_AlasanKunjungan(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_AlasanKunjungan
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> Vw_Select_HubunganPasien(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_HubunganPasien
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> Vw_Select_Skrining(int pageSize, int pageIndex, string filter, int? kelompok = null)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();

                    parameters.Add(new SqlParameter("@Kelompok", (object)kelompok ?? DBNull.Value));
                    wheres.Add("Id_mSkriningKelompok = @Kelompok");

                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_Skrining
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> Vw_Select_StatusPsikologi(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_StatusPsikologi
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Vw_Select_AkutKronis(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("AkutKronis LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            Id AS Value,
                            AkutKronis AS Text
                        FROM
                            mAkutKronis
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}