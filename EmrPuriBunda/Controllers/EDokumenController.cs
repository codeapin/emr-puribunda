﻿using EmrPuriBunda.Constant;
using EmrPuriBunda.Mahas.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EmrPuriBunda.Controllers
{
    [Authorize(Policy = PolicyConstant.InputEDokumen)]
    public class EDokumenController : Controller
    {
        private readonly AppSettings appSettings;

        public EDokumenController(IOptions<AppSettings> options)
        {
            this.appSettings = options.Value;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult DokumenPasien(string nrm)
        {
            var basePath = "";


            var baseUrl = Url.Action("", "EDokumen", null, Request.Scheme, null, null);

            var pathDokumenPasien = $"{basePath}\\{nrm}";

            var model = new DynatreeItem(new DirectoryInfo(pathDokumenPasien), basePath, baseUrl);

            var list = new List<DynatreeItem>() { model };

            var result = JsonConvert.SerializeObject(list);

            return Content(result);
        }

        [Route("EDokumen/{nrm}/{noReg}/{fileName}")]
        public IActionResult GetFile(string nrm, string noReg, string fileName)
        {
            var basePath = "";

            var filePath = $"{basePath}\\{nrm}\\{noReg}\\{fileName}";

            return new PhysicalFileResult(filePath, "application/pdf");
        }
    }

    public class DynatreeItem
    {
        [JsonIgnore]
        private string basePath = "-";
        [JsonIgnore]
        private string baseUrl = "-";

        public string title { get; set; }
        public bool folder { get; set; }
        public string key { get; set; }

        public List<DynatreeItem> children { get; set; }

        public DynatreeItem(FileSystemInfo fsi, string baseUrl, string basePath)
        {
            this.basePath = basePath;
            this.baseUrl = baseUrl;

            title = fsi.Name;
            children = new List<DynatreeItem>();

            if (fsi.Attributes == FileAttributes.Directory)
            {
                folder = true;
                foreach (FileSystemInfo f in (fsi as DirectoryInfo).GetFileSystemInfos())
                {
                    children.Add(new DynatreeItem(f, baseUrl, basePath));;
                }
            }
            else
            {
                folder = false;

                key = ConvertToUrl(fsi.FullName);
            }
        }

        public string JsonToDynatree()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public string ConvertToUrl(string path)
        {
            return path.Replace(baseUrl, basePath).Replace('\\', '/');
        }
    }
}
