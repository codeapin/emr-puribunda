using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using System.IO;
using EmrPuriBunda.Constant;
using EmrPuriBunda.Mahas.Helpers;
using System.Text;

namespace EmrPuriBunda.Controllers
{
    // RoleX
    [Authorize(Policy = PolicyConstant.SadurAsesmen)]
    public class AsesmenDokterRJController : BasePengisianRMController
    {
        public AsesmenDokterRJController(IConfiguration config) : base(config)
        {
        }

        public IActionResult Index(string noReg)
        {
            ViewData["NoReg"] = noReg;

            return View();
        }

        [HttpGet]
        [ActionName("ActionAsesmenDokterRJ")]
        public async Task<JsonResult> Data_AsesmenDokterRJ(string NoReg)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                AsesmenDokterRJ_SetupModel data;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_AsesmenDokterRJ
                        WHERE
                            NoReg=@NoReg
                    ";

                    data = await s.GetData<AsesmenDokterRJ_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoReg", NoReg),
                    });

                    query = $@"
						SELECT
							*
						FROM
							Vw_AsesmenDokterRJ_DiagnosaSekunder
						WHERE
							NoReg=@NoReg
                        ORDER BY 
                            NoUrut
					";

                    if (data != null)
                    {
                        data.BodyDiagramView = data.BodyDiagram != null ? Encoding.UTF8.GetString(data.BodyDiagram) : null;
                        data.tableDiagnosa = await s.GetDatas<AsesmenDokterRJ_DiagnosaSekunder>(query, new List<SqlParameter>
                        {
                            new SqlParameter("@NoReg", NoReg)
                        });
                    }
                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        // RoleX
        [HttpPost]
        [ActionName("ActionAsesmenDokterRJ")]
        public async Task<JsonResult> Post_AsesmenDokterRJ(AsesmenDokterRJ_SetupModel model)
        {
            if (!ModelState.IsValid)
            {
                return InvalidModelResult();
            }

            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            var dokter = await GetDokterAsync();
            var section = Request.GetSection();

            try
            {
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    model.UserId = userid;
                    model.DokterId = dokter.DokterID;
                    model.SectionId = section.SectionID;
                    model.BodyDiagram = !string.IsNullOrEmpty(model.BodyDiagramView) ? Encoding.UTF8.GetBytes(model.BodyDiagramView) : null;

                    s.OpenTransaction();

                    await s.Insert(model);

                    if (model.tableDiagnosa == null) model.tableDiagnosa = new List<AsesmenDokterRJ_DiagnosaSekunder>();
                    foreach (var (x, i) in model.tableDiagnosa.Select((y, i) => (y, i)))
                    {
                        var detailModel = new AsesmenDokterRJ_DiagnosaSekunder()
                        {
                            NoUrut = i + 1,
                            NoReg = model.NoReg,
                            KodeICD = x.KodeICD
                        };

                        await s.Insert(detailModel);
                    }

                    await s.ExecuteNonQuery("EXEC Sp_InsertCpptAsesmenDokterRJ @NoReg", new List<SqlParameter>()
                    {
                        new SqlParameter("@NoReg", model.NoReg)
                    });

                    s.Transaction.Commit();
                }

                if (
                    !string.IsNullOrEmpty(model.Anamnesis) && 
                    (model.tableDiagnosa?.Count > 0 || !string.IsNullOrEmpty(model.DiagnosaUtamaKeterangan)) &&
                    !string.IsNullOrEmpty(model.Planning)
                )
                {
                    await SetPengisianEmrAsync(model.NoReg, AsesmenDokterRJ: true);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }

        // RoleX
        [HttpPut]
        [ActionName("ActionAsesmenDokterRJ")]
        public async Task<JsonResult> Put_AsesmenDokterRJ(string NoReg, AsesmenDokterRJ_SetupModel model)
        {
            if (!ModelState.IsValid)
            {
                return InvalidModelResult();
            }

            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var dokter = await GetDokterAsync();
            var section = Request.GetSection();

            try
            {
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {

                    model.UserId = userid;
                    model.DokterId = dokter.DokterID;
                    model.SectionId = section.SectionID;
                    model.BodyDiagram = !string.IsNullOrEmpty(model.BodyDiagramView) ? Encoding.UTF8.GetBytes(model.BodyDiagramView) : null;

                    var oldModel = new AsesmenDokterRJ_SetupModel();
                    var queryOldModel = $@"
						SELECT
							*
						FROM
							AsesmenDokterRJ
						WHERE
							NoReg=@NoReg";

                    oldModel = await s.GetData<AsesmenDokterRJ_SetupModel>(queryOldModel, new List<SqlParameter> { new SqlParameter("@NoReg", NoReg) });

                    if (oldModel.UserId.Equals(userid, StringComparison.OrdinalIgnoreCase) == false) throw new Exception("Login salah. Hanya user yang menyimpan yang boleh mengubah.");

                    var query = "";
                    query = $@"
						SELECT
							*
						FROM
							AsesmenDokterRJ_DiagnosaSekunder
						WHERE
							NoReg=@NoReg
					";
                    oldModel.tableDiagnosa = await s.GetDatas<AsesmenDokterRJ_DiagnosaSekunder>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoReg", NoReg)
                    });

                    s.OpenTransaction();

                    model.NoReg = oldModel.NoReg;
                    //model.WaktuPelayanan = oldModel.WaktuPelayanan;

                    await s.Update(model);

                    foreach (var x in oldModel.tableDiagnosa)
                    {
                        await s.Delete(x);
                    }

                    model.tableDiagnosa ??= new List<AsesmenDokterRJ_DiagnosaSekunder>();

                    foreach (var (x, i) in model.tableDiagnosa.Select((y, i) => (y, i)))
                    {
                        var detailModel = new AsesmenDokterRJ_DiagnosaSekunder()
                        {
                            NoUrut = i + 1,
                            NoReg = model.NoReg,
                            KodeICD = x.KodeICD
                        };

                        await s.Insert(detailModel);
                    }

                    await s.ExecuteNonQuery("EXEC Sp_UpdateCpptAsesmenDokterRJ @NoReg", new List<SqlParameter>()
                    {
                        new SqlParameter("@NoReg", model.NoReg)
                    });

                    s.Transaction.Commit();
                }

                if (
                    !string.IsNullOrEmpty(model.Anamnesis) &&
                    (model.tableDiagnosa?.Count > 0 || !string.IsNullOrEmpty(model.DiagnosaUtamaKeterangan)) &&
                    !string.IsNullOrEmpty(model.Planning)
                )
                {
                    await SetPengisianEmrAsync(model.NoReg, AsesmenDokterRJ: true);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }

            return Json(new
            {
                Success = true,
                Data = new
                {
                    NoReg = NoReg
                }
            });
        }

        // RoleX
        [HttpDelete]
        [ActionName("ActionAsesmenDokterRJ")]
        public async Task<JsonResult> Delete_AsesmenDokterRJ(string NoReg)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    var model = new AsesmenDokterRJ_SetupModel();
                    var query = "";
                    query = $@"
						SELECT
							*
						FROM
							AsesmenDokterRJ_DiagnosaSekunder
						WHERE
							NoReg=@NoReg
					";
                    model.tableDiagnosa = await s.GetDatas<AsesmenDokterRJ_DiagnosaSekunder>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoReg", NoReg)
                    });

                    s.OpenTransaction();
                    foreach (var x in model.tableDiagnosa) await s.Delete(x);
                    model.NoReg = NoReg;
                    await s.Delete(model);
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }

        [HttpPost]
        public async Task<JsonResult> Vw_Select_AlasanKunjungan(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_AlasanKunjungan
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Vw_Select_HubunganPasien(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_HubunganPasien
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }


        [HttpPost]
        public async Task<JsonResult> LookUp_Diagnosa(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<AsesmenDokterRJ_DiagnosaLookUpModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "KodeICD",
                        "Descriptions"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));
                                wheres.Add($"(KodeICD LIKE @Filter OR Descriptions LIKE @Filter)");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_ICD
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<AsesmenDokterRJ_DiagnosaLookUpModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}