using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using EmrPuriBunda.Constant;

namespace EmrPuriBunda.Controllers
{
    [Authorize(Policy = PolicyConstant.SetupMaster)]
    public class OrderPenunjangKelompokController : Controller
    {
        private IConfiguration Config { get; set; }
        public OrderPenunjangKelompokController(IConfiguration configuration) => Config = configuration;

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_OrderPenunjangKelompok(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<OrderPenunjangKelompok_OrderPenunjangKelompok_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "Nomor",						"Nama",						"JenisID_Text"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Nama":								if (string.IsNullOrEmpty(x.Value)) break;								parameters.Add(new SqlParameter($"@Nama", $"%{x.Value}%"));								wheres.Add($"(Nama LIKE @Nama)");								break;
                            case "Jenis":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Jenis", $"%{x.Value}%"));
                                wheres.Add($"(JenisID_Text LIKE @Jenis)");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_mOrderPenunjang_Kelompok
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<OrderPenunjangKelompok_OrderPenunjangKelompok_ListModel>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpGet]
        [ActionName("OrderPenunjangKelompok")]
        public async Task<JsonResult> Data_OrderPenunjangKelompok(int ID)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                OrderPenunjangKelompok_SetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_mOrderPenunjang_Kelompok
                        WHERE
                            ID=@ID
                    ";

                    data = await s.GetData<OrderPenunjangKelompok_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@ID", ID),
                    });

                    query = $@"						SELECT							*						FROM							Vw_mOrderPenunjang_DetailJasa						WHERE							Kelompok_ID = @ID					";					data.tableJasa = await s.GetDatas<OrderPenunjangKelompok_tableJasa_SetupModel>(query, new List<SqlParameter>					{
                        new SqlParameter("@ID", ID),
                    });
                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        // RoleX
        [HttpPost]
        [ActionName("OrderPenunjangKelompok")]
        public async Task<JsonResult> Post_OrderPenunjangKelompok(OrderPenunjangKelompok_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                try
                {
                    s.OpenTransaction();
                    model.ID = await s.Insert(model, true);
                    if (model.tableJasa == null) model.tableJasa = new List<OrderPenunjangKelompok_tableJasa_SetupModel>();

					foreach (var (x, i) in model.tableJasa.Select((x, i) => (x, i)))					{                        x.Nomor = i + 1;
                        x.Kelompok_ID = model.ID;

						await s.Insert(x);
					}
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpPut]
        [ActionName("OrderPenunjangKelompok")]
        public async Task<JsonResult> Put_OrderPenunjangKelompok(int ID, OrderPenunjangKelompok_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                try
                {                    var headQuery = $@"
                        SELECT
                            *
                        FROM
                            Vw_mOrderPenunjang_Kelompok
                        WHERE
                            ID=@ID
                    ";

                    var oldModel = await s.GetData<OrderPenunjangKelompok_SetupModel>(headQuery, new List<SqlParameter>
                    {
                        new SqlParameter("@ID", ID),
                    });

                    var query = "";					query = $@"						SELECT							*						FROM							mOrderPenunjang_DetailJasa						WHERE							Kelompok_ID = @ID					";					oldModel.tableJasa = await s.GetDatas<OrderPenunjangKelompok_tableJasa_SetupModel>(query, new List<SqlParameter>					{						new SqlParameter("@ID", model.ID)					});
                    oldModel.tableJasa ??= new List<OrderPenunjangKelompok_tableJasa_SetupModel>();
                    model.tableJasa ??= new List<OrderPenunjangKelompok_tableJasa_SetupModel>();

                    s.OpenTransaction();

                    model.ID = ID;
                    await s.Update(model);
					foreach (var old in oldModel.tableJasa)					{                        var oldModelDissapear = model.tableJasa.FirstOrDefault(y => y.JasaID == old.JasaID) == null;
                        if (oldModelDissapear)						{							await s.Delete(old);						}					}
                    foreach (var (x, i) in model.tableJasa.Select((x, i) => (x, i)))					{                        x.Nomor = i + 1;                        x.Kelompok_ID = model.ID;                        var isNewItem = oldModel.tableJasa.FirstOrDefault(y => y.JasaID == x.JasaID) == null;
                        if (isNewItem)						{							await s.Insert(x);						}						else						{							await s.Update(x);						}					}
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpDelete]
        [ActionName("OrderPenunjangKelompok")]
        public async Task<JsonResult> Delete_OrderPenunjangKelompok(int ID)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                try
                {
                    var model = new OrderPenunjangKelompok_SetupModel();

                    var queryHead = $@"
                        SELECT
                            *
                        FROM
                            Vw_mOrderPenunjang_Kelompok
                        WHERE
                            ID=@ID
                    ";

                    model = await s.GetData<OrderPenunjangKelompok_SetupModel>(queryHead, new List<SqlParameter>
                    {
                        new SqlParameter("@ID", ID),
                    });

                    var query = "";					query = $@"						SELECT							*						FROM							mOrderPenunjang_DetailJasa						WHERE							Kelompok_ID = @ID					";					model.tableJasa = await s.GetDatas<OrderPenunjangKelompok_tableJasa_SetupModel>(query, new List<SqlParameter>					{						new SqlParameter("@ID", ID)					});

                    s.OpenTransaction();
                    foreach (var x in model.tableJasa) await s.Delete(x);
                    model.ID = ID;
                    await s.Delete(model);
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }        [HttpPost]
        public async Task<JsonResult> mOrderPenunjang_Jenis(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Nama LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            ID AS Value,
                            Nama AS Text
                        FROM
                            mOrderPenunjang_Jenis
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> LookUp_tableJasa(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<OrderPenunjangKelompok_OrderPenunjangKelompok_tableJasa_LookUpModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "JasaID",						"JasaName"
                    };

                    wheres.Add("Aktif = 1");

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Nama":								if (string.IsNullOrEmpty(x.Value)) break;								parameters.Add(new SqlParameter($"@Nama", $"%{x.Value}%"));								wheres.Add($"JasaName LIKE @Nama OR KeteranganTindakan LIKE @Nama");								break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            SIMmListJasa
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<OrderPenunjangKelompok_OrderPenunjangKelompok_tableJasa_LookUpModel>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}