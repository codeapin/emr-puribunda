using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using EmrPuriBunda.Constant;

namespace EmrPuriBunda.Controllers
{
    [Authorize(Policy = PolicyConstant.SetupMaster)]
    public class SediaanRacikController : Controller
    {
        private IConfiguration Config { get; set; }
        public SediaanRacikController(IConfiguration configuration) => Config = configuration;

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_SediaanRacik(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<SediaanRacik_SediaanRacik_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "NamaRacikan"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":								if (string.IsNullOrEmpty(x.Value)) break;								parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));								wheres.Add($"(NamaRacikan LIKE @Filter)");								break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            SIMmRacikan
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<SediaanRacik_SediaanRacik_ListModel>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpGet]
        [ActionName("SediaanRacik")]
        public async Task<JsonResult> Data_SediaanRacik(string NamaRacikan)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                SediaanRacik_SediaanRacik_SetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            SIMmRacikan
                        WHERE
                            NamaRacikan=@Id
                    ";

                    data = await s.GetData<SediaanRacik_SediaanRacik_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", NamaRacikan),
                    });

                    
                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        // RoleX
        [HttpPost]
        [ActionName("SediaanRacik")]
        public async Task<JsonResult> Post_SediaanRacik(SediaanRacik_SediaanRacik_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                try
                {
                    s.OpenTransaction();
                    await s.Insert(model);
                    
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpDelete]
        [ActionName("SediaanRacik")]
        public async Task<JsonResult> Delete_SediaanRacik(string NamaRacikan)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                try
                {
                    var model = new SediaanRacik_SediaanRacik_SetupModel();
                    

                    s.OpenTransaction();
                    
                    model.NamaRacikan = NamaRacikan;
                    await s.Delete(model);
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }
    }
}