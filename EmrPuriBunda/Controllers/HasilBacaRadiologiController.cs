﻿using EmrPuriBunda.Models;
using Mahas.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EmrPuriBunda.Controllers
{
    [Authorize]
    public class HasilBacaRadiologiController : BaseController
    {
        public HasilBacaRadiologiController(IConfiguration config) : base(config)
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_HasilBacaRadiologi(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<HasilBacaLab_HasilBacaLabModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var wheres = new List<string>()
                    {
                        "SectionID = 'SECT0014'"
                    };

                    var parameters = new List<SqlParameter>();
                    var _orderBy = new List<string>()
                    {
                        "Tanggal",
                        "DokterPengirim",
                        "SectionName",
                        "PenanggungJawab"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            //case "NoReg":
                            //    if (string.IsNullOrEmpty(x.Value)) break;
                            //    parameters.Add(new SqlParameter($"@NoReg", $"{x.Value}"));
                            //    wheres.Add($"RegNo = @NoReg");
                            //    break;
                            case "NRM":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NRM", x.Value));
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            EMR_ListDataHasilPenunjang (@NRM)
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<HasilBacaLab_HasilBacaLabModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}
