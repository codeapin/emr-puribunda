﻿using EmrPuriBunda.Mahas.Helpers;
using EmrPuriBunda.Models;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EmrPuriBunda.Controllers
{
    public class SectionController : BaseController
    {
        public SectionController(IConfiguration config) : base(config)
        {
        }

        [HttpPost]
        public async Task<JsonResult> Datas_SectionAsync(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, string filter, string groupPelayanan)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<mSectionLogin> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "SectionName",
                        "GroupPelayanan"
                    };

                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter($"@Section", $"%{filter}%"));
                        wheres.Add($"(SectionName LIKE @Section OR GroupPelayanan LIKE @Section)");
                    }
                    if (!string.IsNullOrEmpty(groupPelayanan))
                    {
                        parameters.Add(new SqlParameter($"@Group", groupPelayanan));
                        wheres.Add($"GroupPelayanan = @Group");
                    }

                    var query = $@"
                       SELECT
                            GroupPelayanan
                            , SectionID
                            , SectionName
                            , NoUrut
                        FROM 
                            mSectionLogin
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<mSectionLogin>(query, "NoUrut", orderByType, pageIndex, pageSize, parameters);
                }

                var group = datas.GroupBy(x => x.GroupPelayanan).Select(x => new
                {
                    GroupPelayanan = x.Key,
                    Data = x.ToList()
                }).ToList();

                return Json(new
                {
                    Success = true,
                    Datas = group,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPut]
        public async Task<JsonResult> PilihSectionAsync(string sectionId)
        {
            try
            {
                var section = await GetSectionAsync(sectionId);

                if (section == null) throw new Exception("Section tidak ditemukan");

                Response.PutSection(section);

                return Json(new
                {
                    Success = true,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Select_GroupPelayanan(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("GroupPelayanan LIKE @Text");
                    }

                    var query = $@"
                        SELECT 
                            DISTINCT 
                            GroupPelayanan AS Text,
                            GroupPelayanan AS Value 
                        FROM mSectionLogin
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        private async Task<mSectionLogin> GetSectionAsync(string sectionId)
        {
            mSectionLogin data;

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                var parameters = new List<SqlParameter>();
                var wheres = new List<string>();

                var query = $@"
                        SELECT
                            TOP 1
                            SectionID 
                            , SectionName
                            , GroupPelayanan
                            , EResepSectionID
                            , EResepSectionName
                        FROM
                            mSectionLogin
                        WHERE
                            SectionID = @Id
                    ";

                data = await s.GetData<mSectionLogin>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", sectionId),
                    });

            }

            return data;
        }
    }
}
