﻿using EmrPuriBunda.Models;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EmrPuriBunda.Controllers
{
    public class BasePengisianRMController : BaseController
    {
        public BasePengisianRMController(IConfiguration config) : base(config)
        {
        }

        protected async Task SetPengisianEmrAsync(
            string NoReg,
            bool? AsesmenDokterRJ = null,
            bool? AsesmenDokterRI = null,
            bool? AsesmenDokterUGD = null,
            bool? AsesmenPerawatRI = null,
            bool? AsesmenPerawatRJ = null,
            bool? AsesmenPerawatUGD = null,
            bool? CPPT = null)
        {
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    var kelengkapanRM = await s.GetData<KelengkapanDataRM_SetupModel>("EXEC Sp_GetPengisianEMR @NoReg", new List<SqlParameter>
                    {
                        new SqlParameter("@NoReg", NoReg)
                    });

                    s.OpenTransaction();

                    if (AsesmenDokterRJ == true)
                    {
                        kelengkapanRM.AsesmenDokterRJ = AsesmenDokterRJ.Value;
                    }
                    if (AsesmenDokterRI == true)
                    {
                        kelengkapanRM.AsesmenDokterRI = AsesmenDokterRI.Value;
                    }
                    if (AsesmenDokterUGD == true)
                    {
                        kelengkapanRM.AsesmenDokterUGD = AsesmenDokterUGD.Value;
                    }
                    if (AsesmenPerawatRI == true)
                    {
                        kelengkapanRM.AsesmenPerawatRI = AsesmenPerawatRI.Value;
                    }
                    if (AsesmenPerawatRJ == true)
                    {
                        kelengkapanRM.AsesmenPerawatRJ = AsesmenPerawatRJ.Value;
                    }
                    if (AsesmenPerawatUGD == true)
                    {
                        kelengkapanRM.AsesmenPerawatUGD = AsesmenPerawatUGD.Value;
                    }
                    if (CPPT == true)
                    {
                        kelengkapanRM.CPPT = CPPT.Value;
                    }

                    await s.Update(kelengkapanRM);

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
