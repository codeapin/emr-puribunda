using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using System.IO;
using EmrPuriBunda.Constant;

namespace EmrPuriBunda.Controllers
{
    [Authorize(Roles = RolesConstant.Admin)]
    public class TandaTanganController : BaseController
    {
        public TandaTanganController(IConfiguration config) : base(config)
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_TandaTangan(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<TandaTangan_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "DokterId_Text"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":								if (string.IsNullOrEmpty(x.Value)) break;								parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));								wheres.Add($"(Nama LIKE @Filter)");								break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_mProfesiTtd
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<TandaTangan_ListModel>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpGet]
        [ActionName("TandaTangan")]
        public async Task<JsonResult> Data_TandaTangan(string DokterId)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                TandaTangan_SetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_mProfesiTtd
                        WHERE
                            DokterId=@DokterId
                    ";

                    data = await s.GetData<TandaTangan_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@DokterId", DokterId),
                    });

                    
                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        // RoleX
        [HttpPost]
        [ActionName("TandaTangan")]
        public async Task<JsonResult> Post_TandaTangan(TandaTangan_SetupModel model, IFormFile FileTtd)
        {
            if (FileTtd == null)
            {
                return Json(new
                {
                    Success = false,
                    Message = "Dokumen harus diisi"
                });
            }

            if (FileTtd.Length > 5 * 1024 * 1024) ModelState.AddModelError("", $"Ukuran file maksimal 5mb");

            string contentType = FileTtd.ContentType;
            if (!contentType.Equals("image/x-png", StringComparison.OrdinalIgnoreCase)
            && !contentType.Equals("image/gif", StringComparison.OrdinalIgnoreCase)
            && !contentType.Equals("image/jpeg", StringComparison.OrdinalIgnoreCase))
            {
                ModelState.AddModelError("", $"File harus format gambar");
            }

            if (!ModelState.IsValid)
            {
                return InvalidModelResult();
            }

            using (var ms = new MemoryStream())
            {
                FileTtd.CopyTo(ms);
                model.TtdDokter = ms.ToArray();
            }

            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    s.OpenTransaction();
                    await s.Insert(model);
                    
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpPut]
        [ActionName("TandaTangan")]
        public async Task<JsonResult> Put_TandaTangan(string DokterId, TandaTangan_SetupModel model, IFormFile FileTtd)
        {
            if (FileTtd == null)
            {
                return Json(new
                {
                    Success = false,
                    Message = "Dokumen harus diisi"
                });
            }

            if (FileTtd.Length > 5 * 1024 * 1024) ModelState.AddModelError("", $"Ukuran file maksimal 5mb");

            string contentType = FileTtd.ContentType;
            if (!contentType.Equals("image/x-png", StringComparison.OrdinalIgnoreCase)
            && !contentType.Equals("image/gif", StringComparison.OrdinalIgnoreCase)
            && !contentType.Equals("image/jpeg", StringComparison.OrdinalIgnoreCase))
            {
                ModelState.AddModelError("", $"File harus format gambar");
            }

            if (!ModelState.IsValid)
            {
                return InvalidModelResult();
            }

            using (var ms = new MemoryStream())
            {
                FileTtd.CopyTo(ms);
                model.TtdDokter = ms.ToArray();
            }

            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    

                    s.OpenTransaction();
                    model.DokterId = DokterId;
                    await s.Update(model);

                    
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpDelete]
        [ActionName("TandaTangan")]
        public async Task<JsonResult> Delete_TandaTangan(string DokterId)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    var model = new TandaTangan_SetupModel();
                    

                    s.OpenTransaction();
                    
                    model.DokterId = DokterId;
                    await s.Delete(model);
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }

        [HttpPost]
        public async Task<JsonResult> Select_Profesi(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("NamaDOkter LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            DokterID AS Value,
                            NamaDOkter AS Text
                        FROM
                            mDokter
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}