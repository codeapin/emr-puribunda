﻿using EmrPuriBunda.Constant;
using EmrPuriBunda.FileManager;
using EmrPuriBunda.Mahas.Helpers;
using EmrPuriBunda.Models;
using Mahas.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EmrPuriBunda.Controllers
{
    public class DokumenRekamMedisController : BaseController
    {
        private readonly FileManagerService fileManager;

        public DokumenRekamMedisController(IConfiguration config, FileManagerService fileManager) : base(config)
        {
            this.fileManager = fileManager;
        }

        [Authorize(Policy = PolicyConstant.InputEDokumen)]
        public async Task<IActionResult> ViewDokumenAsync(string nrm)
        {
            Pasien_PasienRekamMedis_ListModel pasien;

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                var query = $@"
                        SELECT
                            TOP 1 *
                        FROM
                            mPasien
                        WHERE
                            NRM = @NRM
                    ";

                pasien = await s.GetData<Pasien_PasienRekamMedis_ListModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NRM", nrm),
                    });

                if (pasien == null) return NotFound();
            }

            return View("ViewDokumen", pasien);
        }

        [Authorize(Policy = PolicyConstant.InputEDokumen)]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        [HttpGet]
        [ActionName("DokumenRekamMedis")]
        public async Task<JsonResult> Data_DokumenRekamMedis(string NoBukti)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                DokumenRekamMedis_DokumenRekamMedis_SetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_trDokumenRekamMedis
                        WHERE
                            NoBukti=@NoBukti
                    ";

                    data = await s.GetData<DokumenRekamMedis_DokumenRekamMedis_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoBukti", NoBukti),
                    });

                    query = $@"
						SELECT
							*
						FROM
							trDokumenRekamMedisDetail
						WHERE
							NoBukti = @NoBukti
					";
                    data.tablePathDokumen = await s.GetDatas<DokumenRekamMedis_DokumenRekamMedisDetail_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoBukti", NoBukti),
                    });

                    data.tablePathDokumen?.ForEach(x =>
                    {
                        x.UrlDokumen = ConvertToUrl(x.NoBukti, x.NoUrut.GetValueOrDefault());
                        x.FilePath = "";
                    });

                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<JsonResult> Datas_DokumenRekamMedis(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<DokumenRekamMedis_Header_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "NoBukti",
                        "NRM",
                        "NamaPasien",
                        "TglLahir",
                        "Alamat",
                        "JenisKerjasama",
                        "TanggalDibuat",
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));
                                wheres.Add($"(NRM LIKE @Filter OR NamaPasien LIKE @Filter)");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_trDokumenRekamMedis
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<DokumenRekamMedis_Header_ListModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<JsonResult> ViewDokumenRekamMedis(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

                List<DokumenRekamMedis_Detail_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "NoBukti",
                        "TanggalDibuat",
                        "NoReg",
                        "NRM",
                        "TglReg",
                        "JenisKerjasamaID",
                        "SectionID",
                        "UserID",
                        "RawatJalan",
                        "RawatInap"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "NRM":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NRM", $"{x.Value}"));
                                wheres.Add($"NRM = @NRM");
                                break;
                        }
                    }

                    if (User.IsInRole(RolesConstant.Casemic))
                    {
                        parameters.Add(new SqlParameter($"@Arsip", true));
                        wheres.Add($"ArsipClaimBpjs = 1");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_trDokumenRekamMedisDetail
                        {s.ToWhere(wheres)}
                        ORDER BY
                            TglReg DESC
                    ";

                    datas = await s.GetDatas<DokumenRekamMedis_Detail_ListModel>(query, parameters);

                    datas.ForEach(x => {
                        x.UrlDokumen = Url.Action("GetFile", "DokumenRekamMedis", new { noBukti = x.NoBukti, noUrut = x.NoUrut }, Request.Scheme, null, null);
                    });

                    var groupInfo = datas.GroupBy(x => new { x.Info, x.Tahun, x.InfoTgl }).Select(y => new DokumenRekamMedis_GroupInfo()
                    {
                        Info = y.Key.Info,
                        Tahun = y.Key.Tahun,
                        InfoTgl = y.Key.InfoTgl,
                        GroupClaim = new DokumenRekamMedis_GroupClaim()
                        {
                            ClaimInfo = $"{y.Key.InfoTgl}_Klaim",
                            Datas = y.Where(z => z.ArsipClaimBpjs == true).ToList()
                        },
                        Datas = y.Where(z => z.ArsipClaimBpjs != true).ToList()
                    });

                    var groupTahun = groupInfo.GroupBy(x => x.Tahun).Select(x => new DokumenRekamMedis_GroupTahun()
                    {
                        Tahun = x.Key,
                        GroupInfo = x.ToList()
                    }).ToList();

                    return Json(new
                    {
                        Success = true,
                        Datas = groupTahun
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [Authorize(Policy = PolicyConstant.InputEDokumen)]
        [ActionName("DokumenRekamMedis")]
        public async Task<JsonResult> Post_DokumenRekamMedis(DokumenRekamMedis_DokumenRekamMedis_SetupModel model, List<IFormFile> FileRM)
        {
            if (FileRM == null || FileRM.Count == 0) 
            {
                return Json(new
                {
                    Success = false,
                    Message = "Dokumen harus diisi"
                });
            }

            FileRM.ForEach(x =>
            {
                if (x.Length > 5 * 1024 * 1024) ModelState.AddModelError("", $"Ukuran file maksimal 5mb");
                if (!x.ContentType.Equals("application/pdf", StringComparison.OrdinalIgnoreCase)) ModelState.AddModelError("", $"Ektensi file harus pdf");
            });

            if (!ModelState.IsValid)
            {
                return InvalidModelResult();
            }

            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            var section = Request.GetSection();

            model.SectionID = section.SectionID;
            model.TanggalDibuat = DateTime.Now;
            model.UserID = userid;

            var arsipBpjs = User.IsInRole(RolesConstant.Casemic);
            model.ArsipClaimBpjs = arsipBpjs;

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    model.NoBukti = await Get_NoBuktiAutoIdAsync();

                    var jenisKerjasama = await s.GetData<MahasSelectListItem>(
                        "SELECT TOP 1 JenisKerjasamaID AS Value, JenisKerjasama AS Text FROM Vw_JenisKerjasama WHERE JenisKerjasamaID = @id", new List<SqlParameter>()
                        {
                            new SqlParameter("@id", model.JenisKerjasamaID)
                        });

                    s.OpenTransaction();
                    await s.Insert(model);

                    List<DokumenRekamMedis_DokumenRekamMedisDetail_SetupModel> detail = await fileManager.PostDokumenRekamMedisAsync(model, FileRM, jenisKerjasama.Text);


                    if (detail == null) detail = new List<DokumenRekamMedis_DokumenRekamMedisDetail_SetupModel>();
                    foreach (var x in detail)
                    {
                        await s.Insert(x);
                    }

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }

        [Authorize(Policy = PolicyConstant.InputEDokumen)]
        [HttpPut]
        [ActionName("DokumenRekamMedis")]
        public async Task<JsonResult> Put_DokumenRekamMedis(string NoBukti, DokumenRekamMedis_DokumenRekamMedis_SetupModel model, List<IFormFile> FileRM, List<int> noUrutEdit)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            FileRM.ForEach(x =>
            {
                if (x.Length > 5 * 1024 * 1024) ModelState.AddModelError("", $"Ukuran file maksimal 5mb");
                if (!x.ContentType.Equals("application/pdf", StringComparison.OrdinalIgnoreCase)) ModelState.AddModelError("", $"Ektensi file harus pdf");
            });

            if (!ModelState.IsValid)
            {
                return InvalidModelResult();
            }

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    var oldModel = new DokumenRekamMedis_DokumenRekamMedis_SetupModel();

                    var queryHeader = "";
                    queryHeader = $@"
						SELECT
							*
						FROM
							Vw_trDokumenRekamMedis
						WHERE
							NoBukti = @NoBukti
					";

                    var jenisKerjasama = await s.GetData<MahasSelectListItem>(
                        "SELECT TOP 1 JenisKerjasamaID AS Value, JenisKerjasama AS Text FROM Vw_JenisKerjasama WHERE JenisKerjasamaID = @id", new List<SqlParameter>()
                        {
                            new SqlParameter("@id", model.JenisKerjasamaID)
                        });

                    var paramNoBukti = new List<SqlParameter>
                    {
                        new SqlParameter("@NoBukti", NoBukti)
                    };

                    oldModel = await s.GetData<DokumenRekamMedis_DokumenRekamMedis_SetupModel>(queryHeader, paramNoBukti);

                    if (oldModel == null) throw new Exception("Data tidak ditemukan");

                    oldModel.NoReg = model.NoReg;
                    oldModel.NRM = model.NRM;
                    oldModel.TglReg = model.TglReg;
                    oldModel.TglReg = model.TglReg;
                    oldModel.RawatInap = model.RawatInap;
                    oldModel.RawatJalan = model.RawatJalan;
                    oldModel.JenisKerjasamaID = model.JenisKerjasamaID;
                    oldModel.Catatan = model.Catatan;
                    oldModel.UserID = model.UserID;


                    var detail = await s.GetDatas<DokumenRekamMedis_DokumenRekamMedisDetail_SetupModel>("SELECT * FROM trDokumenRekamMedisDetail WHERE NoBukti = @NoBukti", new List<SqlParameter>()
                    {
                        new SqlParameter("NoBukti", NoBukti)
                    });


                    var fileToDelete = detail.Where(x => !noUrutEdit.Contains(x.NoUrut.GetValueOrDefault())).ToList();
                    var fileToKeep = detail.Where(x => noUrutEdit.Contains(x.NoUrut.GetValueOrDefault())).ToList();

                    var maxFileDelete = fileToDelete.Count > 0 ? fileToDelete.Max(x => x.NoUrut) : 0;
                    var maxFileToKeep = fileToKeep.Count > 0 ? fileToKeep.Max(x => x.NoUrut) : 0;

                    int noUrutMax = maxFileDelete > maxFileToKeep ? (int)maxFileDelete : (int)maxFileToKeep;
                    noUrutMax = noUrutMax == 0 ? 1 : noUrutMax;

                    model.tablePathDokumen = await fileManager.PostDokumenRekamMedisAsync(model, FileRM, jenisKerjasama.Text, fileToDelete.Select(x => x.FilePath).ToList(), noUrutMax);

                    s.OpenTransaction();

                    await s.Update(oldModel);

                    foreach (var item in fileToDelete)
                    {
                        await s.Delete(item);
                    }

                    if (model.tablePathDokumen == null) model.tablePathDokumen = new List<DokumenRekamMedis_DokumenRekamMedisDetail_SetupModel>();

                    foreach (var x in model.tablePathDokumen)
                    {
                        await s.Insert(x);
                    }

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }

        [Authorize(Roles = "Admin")]
        // RoleX
        [HttpDelete]
        [ActionName("ViewDokumenRekamMedis")]
        public async Task<JsonResult> Delete_DokumenRekamMedis(string NoBukti, int NoUrut)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    var model = new DokumenRekamMedis_DokumenRekamMedisDetail_SetupModel();

                    var query = "SELECT FilePath FROM trDokumenRekamMedisDetail WHERE NoBukti = @NoBukti AND NoUrut = @NoUrut";

                    var data = await s.GetData<DokumenRekamMedis_DokumenRekamMedisDetail_SetupModel>(query, new List<SqlParameter>()
                    {
                        new SqlParameter("@NoBukti", NoBukti),
                        new SqlParameter("@NoUrut", NoUrut),
                    });

                    var filePath = data.FilePath;

                    await fileManager.DeleteDokumenRekamMedisAsync(filePath);

                    s.OpenTransaction();

                    model.NoBukti = NoBukti;
                    model.NoUrut = NoUrut;

                    await s.Delete(model);
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }

        [HttpDelete]
        [ActionName("DokumenRekamMedis")]
        public async Task<JsonResult> Delete_HasilBacaPenunjang(string NoBukti)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    var model = new DokumenRekamMedis_DokumenRekamMedis_SetupModel();

                    var query = "";
                    query = $@"
						SELECT
							*
						FROM
							trDokumenRekamMedisDetail
						WHERE
							NoBukti=@NoBukti
					";
                    model.tablePathDokumen = await s.GetDatas<DokumenRekamMedis_DokumenRekamMedisDetail_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoBukti", NoBukti)
                    });

                    s.OpenTransaction();
                    foreach (var x in model.tablePathDokumen) await s.Delete(x);
                    model.NoBukti = NoBukti;
                    await s.Delete(model);
                    s.Transaction.Commit();

                    foreach (var x in model.tablePathDokumen)
                    {
                        await fileManager.DeleteDokumenRekamMedisAsync(x.FilePath);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }

        [Authorize]
        [ActionName("GetFile")]
        [Route("DokumenRekamMedis/File/{noBukti}/{noUrut}")]
        public async Task<IActionResult> GetFileAsync(string noBukti, int noUrut)
        {
            using var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection"));

            var query = "SELECT FilePath FROM trDokumenRekamMedisDetail WHERE NoBukti = @NoBukti AND NoUrut = @NoUrut";

            var data = await s.GetData<DokumenRekamMedis_DokumenRekamMedisDetail_SetupModel>(query, new List<SqlParameter>()
                {
                    new SqlParameter("@NoBukti", noBukti),
                    new SqlParameter("@NoUrut", noUrut),
                });

            var result = fileManager.GetDokumenRekamMedis(data.FilePath);

            return File(result, "application/pdf");
        }

        [HttpPost]
        public async Task<JsonResult> Vw_JenisKerjasama(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("JenisKerjasama LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            JenisKerjasamaID AS [Value],
                            JenisKerjasama AS [Text]
                        FROM
                            Vw_JenisKerjasama
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> LookUp_lookupPasien(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<Pasien_PasienRekamMedis_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "NRM",
                        "NamaPasien",
                        "JenisKelamin",
                        "TglLahir",
                        "Alamat"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));
                                wheres.Add($"(NamaPasien LIKE @Filter OR NRM LIKE @Filter )");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            mPasien
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<Pasien_PasienRekamMedis_ListModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> LookUp_lookupRegistrasi(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<DokumenRekamMedis_lookupRegistrasi_LookUpModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "NoReg",
                        "TglReg",
                        "NRM",
                        "RawatInap",
                        "NamaPasien",
                        "JenisKelamin",
                        "JenisKerjasama",
                        "JenisKerjasamaID",
                        "Alamat",
                        "TglLahir",
                        "NoSEP"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "NRM":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NRM", x.Value));
                                wheres.Add($"(NRM = @NRM)");
                                break;
                            case "NoReg":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NoReg", $"%{x.Value}%"));
                                wheres.Add($"(NoReg LIKE @NoReg)");
                                break;
                            case "NoSEP":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NoSEP", $"%{x.Value}%"));
                                wheres.Add($"(NoSEP LIKE @NoSEP)");
                                break;
                            case "NamaPasien":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NamaPasien", $"%{x.Value}%"));
                                wheres.Add($"(NamaPasien LIKE @NamaPasien)");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            NoReg
                            , 'TglReg' = COALESCE(TglKeluar, TglReg)
                            , NoSEP
                            , NRM
                            , RawatInap
                            , RawatJalan
                            , TglKeluar
                            , JenisKelamin
                            , Alamat
                            , NamaPasien
                            , TglLahir
                            , JenisPasien
                            , Kerjasama
                            , JenisKerjasama
                            , JenisKerjasamaID
                        FROM
                            Vw_Registrasi
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<DokumenRekamMedis_lookupRegistrasi_LookUpModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpGet]
        public async Task<JsonResult> AutoId_noBukti()
        {
            try
            {
                var id = await Get_NoBuktiAutoIdAsync();

                return Json(new
                {
                    Success = true,
                    Id = id
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        private async Task<string> Get_NoBuktiAutoIdAsync()
        {
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                var dateCode = "yyyyMMdd";
                var baseCode = "@{DATE}RMD-######";
                var tempCode = MahasConverter.AutoIdGetTempCode(baseCode, dateCode);

                var parameters = new List<SqlParameter>()
                    {
                        new SqlParameter("@NoBukti", $"%{tempCode}%")
                    };
                var wheres = new List<string>()
                    {
                        "NoBukti LIKE @NoBukti"
                    };

                var query = $@"
                        SELECT
                            *
                        FROM
                            trDokumenRekamMedis
                        {s.ToWhere(wheres)}
                        ORDER BY NoBukti DESC
                    ";

                var data = await s.GetData<DokumenRekamMedis_DokumenRekamMedis_SetupModel>(query, parameters);
                string id = MahasConverter.AutoId(baseCode, dateCode, data?.NoBukti);
                return id;
            }
        }

        private string ConvertToUrl(string noBukti, int noUrut)
        {
            return Url.Action("GetFile", "DokumenRekamMedis", new { noBukti, noUrut }, Request.Scheme, null, null);
        }
    }
}
