using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using EmrPuriBunda.Constant;

namespace EmrPuriBunda.Controllers
{
    [Authorize(Policy = PolicyConstant.SetupMaster)]
    public class OrderPenunjangJenisController : Controller
    {
        private IConfiguration Config { get; set; }
        public OrderPenunjangJenisController(IConfiguration configuration) => Config = configuration;

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_OrderPenunjangJenis(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<OrderPenunjangJenis_OrderPenunjangJenis_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "Nomor",						"Nama",						"SectionID_Text"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Nama":								if (string.IsNullOrEmpty(x.Value)) break;								parameters.Add(new SqlParameter($"@Nama", $"%{x.Value}%"));								wheres.Add($"(Nama LIKE @Nama)");								break;
                            case "Section":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Section", $"%{x.Value}%"));
                                wheres.Add($"(SectionID_Text LIKE @Section)");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_mOrderPenunjang_Jenis
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<OrderPenunjangJenis_OrderPenunjangJenis_ListModel>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpGet]
        [ActionName("OrderPenunjangJenis")]
        public async Task<JsonResult> Data_OrderPenunjangJenis(int ID)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                OrderPenunjangJenis_OrderPenunjangJenis_SetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_mOrderPenunjang_Jenis
                        WHERE
                            ID=@ID
                    ";

                    data = await s.GetData<OrderPenunjangJenis_OrderPenunjangJenis_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@ID", ID),
                    });

                    
                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        // RoleX
        [HttpPost]
        [ActionName("OrderPenunjangJenis")]
        public async Task<JsonResult> Post_OrderPenunjangJenis(OrderPenunjangJenis_OrderPenunjangJenis_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                try
                {
                    s.OpenTransaction();
                    model.ID = await s.Insert(model, true);
                    
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpPut]
        [ActionName("OrderPenunjangJenis")]
        public async Task<JsonResult> Put_OrderPenunjangJenis(int ID, OrderPenunjangJenis_OrderPenunjangJenis_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                try
                {
                    

                    s.OpenTransaction();
                    model.ID = ID;
                    await s.Update(model);

                    
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpDelete]
        [ActionName("OrderPenunjangJenis")]
        public async Task<JsonResult> Delete_OrderPenunjangJenis(int ID)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
            {
                try
                {
                    var model = new OrderPenunjangJenis_OrderPenunjangJenis_SetupModel();
                    

                    s.OpenTransaction();
                    
                    model.ID = ID;
                    await s.Delete(model);
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }        [HttpPost]
        public async Task<JsonResult> Select_Section(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("SectionName LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            SectionID AS Value,
                            SectionName AS Text
                        FROM
                            SIMmSection
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}