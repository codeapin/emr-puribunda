using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using EmrPuriBunda.Mahas.Helpers;
using EmrPuriBunda.Constant;

namespace EmrPuriBunda.Controllers
{
    [Authorize(Policy = PolicyConstant.Cppt)]
    public class CpptController : BasePengisianRMController
    {
        public CpptController(IConfiguration config) : base(config)
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_Cppt(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<Cppt_CpptModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "Tanggal",
                        "Id_mCpptTipe_Text",
                        "Dpjp_Text"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "NRM":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NRM", x.Value));
                                wheres.Add($"(NRM = @NRM)");
                                break;
                            case "DariTanggal":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@DariTanggal", x.Value));
                                wheres.Add($"(CAST(Tanggal AS DATE) >= @DariTanggal)");
                                break;
                            case "SampaiTanggal":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@SampaiTanggal", x.Value));
                                wheres.Add($"(CAST(Tanggal AS DATE) <= @SampaiTanggal)");
                                break;
                            case "Profesi":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@Profesi", $"%{x.Value}%"));
                                wheres.Add($"(ProfesiRole LIKE @Profesi)");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Cppt
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<Cppt_CpptModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> RiwayatKunjungan_Cppt(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<Cppt_CpptModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "Tanggal",
                        "Id_mCpptTipe_Text",
                        "Dpjp_Text"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "NRM":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NRM", x.Value));
                                break;
                            case "SemuaRiwayat":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@SemuaRiwayat", x.Value));
                                break;
                            case "HanyaDokter":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@DokterOnly", x.Value));
                                break;
                            case "HanyaRJ":
                                parameters.Add(new SqlParameter($"@RJOnly", x.Value));
                                if (string.IsNullOrEmpty(x.Value)) break;
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            dbo.Ft_GetRiwayatKunjunganCppt(@NRM, @SemuaRiwayat, @DokterOnly, @RJOnly)
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<Cppt_CpptModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> History_Cppt(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<Cppt_CpptModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "Tanggal",
                        "Id_mCpptTipe_Text",
                        "Dpjp_Text"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "IdCppt":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@IdCppt", x.Value));
                                wheres.Add($"(IdCppt = @IdCppt)");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Cppt_History
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<Cppt_CpptModel>(query, _orderBy[orderBy], orderByType, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpGet]
        [ActionName("ActionCppt")]
        public async Task<JsonResult> Data_Cppt(int Id)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                Cppt_CpptSetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Cppt
                        WHERE
                            Id = @Id
                    ";

                    data = await s.GetData<Cppt_CpptSetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", Id),
                    });


                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        // RoleX
        [HttpPost]
        [ActionName("ActionCppt")]
        public async Task<JsonResult> Post_Cppt(Cppt_CpptSetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            bool disposisi;

            try
            {
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    try
                    {
                        model.LastUpdate = DateTime.Now;
                        model.SectionId = Request.GetSection()?.SectionID;
                        model.UserId = userid;
                        model.Keluar_Tanggal = model.Tanggal;
                        model.SumberData = "CPPT";

                        s.OpenTransaction();

                        await s.Insert(model);

                        disposisi = model.Id_mKondisiKeluar != null;


                        s.Transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    try
                    {
                        if (disposisi)
                        {
                            s.OpenTransaction();

                            await s.ExecuteNonQuery("UPDATE SIM_PB.dbo.SIMtrDataRegPasien SET DisposisiCppt = ISNULL(@Disposisi, 0) WHERE NoReg = @NoReg AND Nomor = @Nomor", new List<SqlParameter>()
                            {
                                new SqlParameter("@NoReg", model.NoReg),
                                new SqlParameter("@Nomor", model.Nomor),
                                new SqlParameter("@Disposisi", disposisi),
                            });

                            s.CommitTransaction();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                if (
                    !string.IsNullOrEmpty(model.Subjective) &&
                    !string.IsNullOrEmpty(model.Assesment) &&
                    !string.IsNullOrEmpty(model.Planning)
                )
                {
                    await SetPengisianEmrAsync(model.NoReg, CPPT: true);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }

        // RoleX
        [HttpPut]
        [ActionName("ActionCppt")]
        public async Task<JsonResult> Put_Cppt(int Id, Cppt_CpptSetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            Cppt_CpptSetupModel oldModel;

            bool disposisi;

            try
            {
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    try
                    {
                        var query = $@"
                            SELECT
                                *
                            FROM
                                Vw_Cppt
                            WHERE
                                Id = @Id
                        ";

                        oldModel = await s.GetData<Cppt_CpptSetupModel>(query, new List<SqlParameter>
                        {
                            new SqlParameter("@Id", Id)
                        });
                        if (oldModel == null) throw new Exception("Data tidak ditemukan");
                        if (!oldModel.SumberData.Equals("CPPT", StringComparison.OrdinalIgnoreCase)) throw new Exception("Tidak dapat menyimpan. Sumber data bukan dari CPPT.");

                        s.OpenTransaction();

                        await s.ExecuteNonQuery("exec Sp_InsertCpptHistory @IdCppt", new List<SqlParameter>()
                        {
                            new SqlParameter("@IdCppt", oldModel.Id)
                        });

                        model.Id = oldModel.Id;
                        model.Tanggal = oldModel.Tanggal;
                        model.NoReg = oldModel.NoReg;
                        model.SectionId = oldModel.SectionId;
                        model.UserId = userid;
                        model.LastUpdate = DateTime.Now;
                        model.AdaPerubahan = true;
                        model.Keluar_Tanggal = oldModel.Tanggal;
                        model.SumberData = "CPPT";

                        await s.Update(model);

                        disposisi = model.Id_mKondisiKeluar != null;

                        if (model.Id_mKondisiKeluar != oldModel.Id_mKondisiKeluar)
                        {
                            await s.ExecuteNonQuery("EXEC dbo.Sp_UpdateDisposisiCPPT @NoReg, @Nomor, @Disposisi", new List<SqlParameter>()
                            {
                                new SqlParameter("@NoReg", model.NoReg),
                                new SqlParameter("@Nomor", model.Nomor),
                                new SqlParameter("@Disposisi", disposisi),
                            });
                        }

                        s.Transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    try
                    {
                        if (disposisi)
                        {
                            s.OpenTransaction();

                            await s.ExecuteNonQuery("UPDATE SIM_PB.dbo.SIMtrDataRegPasien SET DisposisiCppt = ISNULL(@Disposisi, 0) WHERE NoReg = @NoReg AND Nomor = @Nomor", new List<SqlParameter>()
                            {
                                new SqlParameter("@NoReg", model.NoReg),
                                new SqlParameter("@Nomor", model.Nomor),
                                new SqlParameter("@Disposisi", disposisi),
                            });

                            s.CommitTransaction();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                if (
                    !string.IsNullOrEmpty(model.Subjective) &&
                    !string.IsNullOrEmpty(model.Assesment) &&
                    !string.IsNullOrEmpty(model.Planning)
                )
                {
                    await SetPengisianEmrAsync(model.NoReg, CPPT: true);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }

            return Json(new
            {
                Success = true
            });
        }

        [HttpPut]
        [ActionName("VerifikasiSemuaCppt")]
        public async Task<JsonResult> Verifikasi_Cppt(string NoReg)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var dokter = await GetDokterAsync();

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        UPDATE 
                            Cppt
                        SET 
                            Verifikasi = 1,
                            Verifikasi_Tanggal = GETDATE()
                        WHERE
                            Dpjp = @Dpjp
                            AND NoReg = @NoReg
                    ";

                    s.OpenTransaction();

                    await s.ExecuteNonQuery(query, new List<SqlParameter>()
                    {
                        new SqlParameter("@Dpjp", dokter.DokterID),
                        new SqlParameter("@NoReg", NoReg)
                    });

                    s.CommitTransaction();
                }
                return Json(new
                {
                    Success = true
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPut]
        [ActionName("VerifikasiCppt")]
        public async Task<JsonResult> Verifikasi_Cppt(int id)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var dokter = await GetDokterAsync();

                Cppt_CpptSetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            Cppt
                        WHERE
                            Id = @Id
                    ";

                    data = await s.GetData<Cppt_CpptSetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", id),
                    });

                    if (data == null) throw new Exception("Data tidak ditemukan");
                    if (data.Dpjp.Equals(dokter?.DokterID, StringComparison.OrdinalIgnoreCase) == false) throw new Exception("Login salah. Harus diverifikasi oleh pihak yang bersangkutan.");

                    data.Verifikasi = true;
                    data.Verifikasi_Tanggal = DateTime.Now;

                    s.OpenTransaction();
                    await s.Update(data);
                    s.CommitTransaction();

                }
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPut]
        [ActionName("KonfirmasiCppt")]
        public async Task<JsonResult> Konfirmasi_Cppt(int id)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var dokter = await GetDokterAsync(userid);
                Cppt_CpptSetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            Cppt
                        WHERE
                            Id = @Id
                    ";

                    data = await s.GetData<Cppt_CpptSetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", id),
                    });

                    if (data == null) throw new Exception("Data tidak ditemukan");
                    if (data.Konfirmasi_Dpjp.Equals(dokter?.DokterID, StringComparison.OrdinalIgnoreCase) == false) throw new Exception("Login salah. Harus dikonfirmasi oleh pihak yang bersangkutan.");

                    data.SudahKonfirmasi = true;
                    data.Konfirmasi_Tanggal = DateTime.Now;

                    s.OpenTransaction();
                    await s.Update(data);
                    s.CommitTransaction();
                }
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        // RoleX
        [HttpDelete]
        [ActionName("ActionCppt")]
        public async Task<JsonResult> Delete_Cppt(int id)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    var model = new Cppt_CpptSetupModel();


                    s.OpenTransaction();

                    model.Id = id;
                    await s.Delete(model);
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }

        [HttpPost]
        public async Task<JsonResult> Vw_Select_Profesi(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_Profesi
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Vw_Select_Dokter(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_Dokter
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Vw_Select_KondisiKeluar(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_KondisiKeluar
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Vw_Select_CaraKeluar(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_CaraKeluar
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Vw_Select_Tipe(int pageSize, int pageIndex, string filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<MahasSelectListItem> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        parameters.Add(new SqlParameter("@Text", $"%{filter}%"));
                        wheres.Add("Text LIKE @Text");
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Select_CpptTipe
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<MahasSelectListItem>(query, "Text", OrderByTypeEnum.ASC, pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}