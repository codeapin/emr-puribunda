using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;
using EmrPuriBunda.Constant;

namespace EmrPuriBunda.Controllers
{
    [Authorize(Roles = RolesConstant.Admin)]
    public class RoleController : Controller
    {
        private readonly RoleManager<IdentityRole> RoleManager;
        private IConfiguration Config { get; set; }

        public RoleController(IConfiguration configuration, RoleManager<IdentityRole> roleManager)
        {
            Config = configuration;
            RoleManager = roleManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_Role(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<Role_RoleModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("UserIdentity")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "Name"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":
								if (string.IsNullOrEmpty(x.Value)) break;
								parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));
								wheres.Add($"Name LIKE @Filter");
								break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            AspNetRoles
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<Role_RoleModel>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpGet]
        [ActionName("ActionRole")]
        public async Task<JsonResult> Data_Role(string Id)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                Role_RoleSetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("UserIdentity")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            AspNetRoles
                        WHERE
                            Id=@Id
                    ";

                    data = await s.GetData<Role_RoleSetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@Id", Id),
                    });

                    
                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        // RoleX
        [HttpPost]
        [ActionName("ActionRole")]
        public async Task<JsonResult> Post_Role(Role_RoleSetupModel model)
        {
            try
            {
                //var roleExist = await roleManager.RoleExistsAsync(model.Name);
                //if (!roleExist)
                //{
                await RoleManager.CreateAsync(new IdentityRole
                    {
                        Name = model.Name
                    });
                //}

                return Json(new
                {
                    Success = true,
                    Data = model
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        // RoleX
        [HttpPut]
        [ActionName("ActionRole")]
        public async Task<JsonResult> Put_Role(string Id, Role_RoleSetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("UserIdentity")))
            {
                try
                {
                    

                    s.OpenTransaction();
                    model.Id = Id;
                    await s.Update(model);

                    
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }

        // RoleX
        [HttpDelete]
        [ActionName("ActionRole")]
        public async Task<JsonResult> Delete_Role(string Id)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("UserIdentity")))
            {
                try
                {
                    var model = new Role_RoleSetupModel();
                    

                    s.OpenTransaction();
                    
                    model.Id = Id;
                    await s.Delete(model);
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }
    }
}