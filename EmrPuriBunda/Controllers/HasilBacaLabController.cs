using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace EmrPuriBunda.Controllers
{
    [Authorize]
    public class HasilBacaLabController : Controller
    {
        private IConfiguration Config { get; set; }
        public HasilBacaLabController(IConfiguration configuration) => Config = configuration;

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_HasilBacaLab(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<HasilBacaLab_HasilBacaLabModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    //TODO : SECTION HARUS BUAT CONFIGNYA AGAR DINAMIS

                    var wheres = new List<string>()
                    {
                        "SectionID IN ('SEC005','SECT0020')"
                    };

                    var parameters = new List<SqlParameter>();
                    var _orderBy = new List<string>()
                    {
                        "Tanggal",						"DokterPengirim",						"SectionName",						"PenanggungJawab"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            //case "NoReg":
                            //    if (string.IsNullOrEmpty(x.Value)) break;
                            //    parameters.Add(new SqlParameter($"@NoReg", $"{x.Value}"));
                            //    wheres.Add($"RegNo = @NoReg");
                            //    break;
                            case "NRM":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NRM", x.Value));
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            EMR_ListDataHasilPenunjang (@NRM)
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<HasilBacaLab_HasilBacaLabModel>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpGet]
        [ActionName("ActionHasilBacaLab")]
        public async Task<JsonResult> Data_HasilBacaLab(string NoReg, string NoSystem)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                HasilBacaLab_HasilBacaLabSetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var query = $@"
                        SELECT
                            TOP 1 *
                        FROM
                            EMR_VIEW_LABORATORIUM
                        WHERE
                            RegNo=@NoReg AND
                            NoSystem = @NoSystem
                    ";

                    data = await s.GetData<HasilBacaLab_HasilBacaLabSetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@NoSystem", NoSystem),
                        new SqlParameter("@NoReg", NoReg)
                    });

                    
                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}