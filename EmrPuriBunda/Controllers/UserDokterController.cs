using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using EmrPuriBunda.Constant;

namespace EmrPuriBunda.Controllers
{
    [Authorize(Roles = RolesConstant.Admin)]
    public class UserDokterController : Controller
    {
        private IConfiguration Config { get; set; }
        public UserDokterController(IConfiguration configuration) => Config = configuration;

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_UserDokter(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<UserDokter_UserDokter_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "UserName",						"Nama_Supplier"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":								if (string.IsNullOrEmpty(x.Value)) break;								parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));								wheres.Add($"(UserName LIKE @Filter OR  Nama_Supplier LIKE @Filter)");								break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_UserDokter
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<UserDokter_UserDokter_ListModel>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpGet]
        [ActionName("UserDokter")]
        public async Task<JsonResult> Data_UserDokter(string UserId)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                UserDokter_UserDokter_SetupModel data;
                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_UserDokter
                        WHERE
                            UserId=@UserId
                    ";

                    data = await s.GetData<UserDokter_UserDokter_SetupModel>(query, new List<SqlParameter>
                    {
                        new SqlParameter("@UserId", UserId),
                    });

                    
                }
                if (data == null) throw new Exception("Data tidak ditemukan");
                return Json(new
                {
                    Success = true,
                    Data = data,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        // RoleX
        [HttpPost]
        [ActionName("UserDokter")]
        public async Task<JsonResult> Post_UserDokter(UserDokter_UserDokter_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    s.OpenTransaction();
                    await s.Insert(model);
                    
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpPut]
        [ActionName("UserDokter")]
        public async Task<JsonResult> Put_UserDokter(string UserId, UserDokter_UserDokter_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    

                    s.OpenTransaction();
                    model.UserId = UserId;
                    await s.Update(model);

                    
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }        // RoleX
        [HttpDelete]
        [ActionName("UserDokter")]
        public async Task<JsonResult> Delete_UserDokter(string UserId)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    var model = new UserDokter_UserDokter_SetupModel();
                    

                    s.OpenTransaction();
                    
                    model.UserId = UserId;
                    await s.Delete(model);
                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true
            });
        }        [HttpPost]
        public async Task<JsonResult> LookUp_userDokter_lookup1(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<UserDokter_UserDokter_LookUpUser> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "Id",						"UserName",						"Email",						"PhoneNumber"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Filter":								if (string.IsNullOrEmpty(x.Value)) break;								parameters.Add(new SqlParameter($"@Filter", $"%{x.Value}%"));								wheres.Add($"(UserName LIKE @Filter OR Email LIKE @Filter OR PhoneNumber LIKE @Filter)");								break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_User
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<UserDokter_UserDokter_LookUpUser>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }        [HttpPost]
        public async Task<JsonResult> LookUp_userDokter_lookup2(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<User_LookupDokter> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "DokterID",						"NamaDOkter",						"Alamat",						"NoKontak",						"Active"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "Nama":								if (string.IsNullOrEmpty(x.Value)) break;								parameters.Add(new SqlParameter($"@Nama", $"%{x.Value}%"));								wheres.Add($"NamaDOkter LIKE @Nama");								break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            mDokter
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<User_LookupDokter>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}