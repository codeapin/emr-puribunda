using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace EmrPuriBunda.Controllers
{
    [Authorize]
    public class HasilBacaLabDetailController : BaseController
    {
        public HasilBacaLabDetailController(IConfiguration config) : base(config)
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [ActionName("ActionHasilBacaLabDetail")]
        public async Task<JsonResult> Data_HasilBacaLabDetail(string NoBukti)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

                HasilBacaLabDetail_SetupModel result = new HasilBacaLabDetail_SetupModel();

                using (var r = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    result.ReviewHasil = await r.GetData<InputHasilPenunjang_KomentarPenunjang_SetupModel>("SELECT * FROM trHasilBacaPenunjangReview WHERE NoBukti = @NoBukti", new List<SqlParameter>()
                    {
                        new SqlParameter("@NoBukti", NoBukti)
                    });

                    if(result.ReviewHasil == null)
                    {
                        result.ReviewHasil = new InputHasilPenunjang_KomentarPenunjang_SetupModel()
                        {
                            NoBukti = NoBukti
                        };
                    }
                }

                using (var s = new MahasConnection(Config.GetConnectionString("SIMConnection")))
                {
                    var query = $@"
                        SELECT
                            *
                        FROM
                            EMR_VIEW_LABORATORIUM_DETAIL
                        WHERE
                            NoSystem = @NoBukti
                    ";

                    var datas = await s.GetDatas<HasilBacaLabDetail_HasilBacaLabDetailModel>(query, new List<SqlParameter>() 
                    {
                        new SqlParameter("@NoBukti", NoBukti)
                    });

                    result.GroupKategori = datas.GroupBy(x => x.KategoriTestNama).Select(y => new HasilBacaLabDetail_GroupKategori
                    {
                        Kategori = y.Key,
                        Hasil = y.ToList()
                    }).ToList();
                }

                return Json(new
                {
                    Success = true,
                    Data = result,
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpPut]
        [ActionName("ActionHasilBacaLabDetail")]
        public async Task<JsonResult> Put_HasilBacaLabDetail(string NoBukti, InputHasilPenunjang_KomentarPenunjang_SetupModel model)
        {
            var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);

            var dokter = await GetDokterAsync(userid);

            model.DokterID = dokter.DokterID;

            using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
            {
                try
                {
                    string query = @"
                        IF(EXISTS(SELECT TOP 1 NoBukti FROM trHasilBacaPenunjangReview WHERE NoBukti = @NoBukti))
                        BEGIN
	
	                        UPDATE trHasilBacaPenunjangReview SET 
		                        Dibaca = @Dibaca,
		                        Komentar = @Komentar,
		                        DokterID = @DokterID
	                        WHERE 
		                        NoBukti = @NoBukti

                        END
                        ELSE
                        BEGIN

	                        INSERT INTO trHasilBacaPenunjangReview VALUES (@NoBukti, @Dibaca, @Komentar, @DokterID)

                        END
                    ";

                    s.OpenTransaction();

                    await s.ExecuteNonQuery(query, new List<SqlParameter>()
                    {
                        new SqlParameter("@NoBukti", (object) NoBukti ?? DBNull.Value),
                        new SqlParameter("@Dibaca", (object) model.Dibaca ?? DBNull.Value),
                        new SqlParameter("@Komentar", (object) model.Komentar ?? DBNull.Value),
                        new SqlParameter("@DokterID", (object) model.DokterID ?? DBNull.Value),
                    });

                    s.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        ex.Message
                    });
                }
            }

            return Json(new
            {
                Success = true,
                Data = model
            });
        }
    }
}