using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Authorization;
using Mahas.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace EmrPuriBunda.Controllers
{
    [Authorize]
    public class ObatController : Controller
    {
        private IConfiguration Config { get; set; }
        public ObatController(IConfiguration configuration) => Config = configuration;

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Datas_Obat(int pageSize, int pageIndex, int orderBy, OrderByTypeEnum orderByType, List<FilterModel> filter)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                int totalRowCount;
                List<Obat_Obat_ListModel> datas;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    var parameters = new List<SqlParameter>();
                    var wheres = new List<string>();
                    var _orderBy = new List<string>()
                    {
                        "Tanggal",						"NamaDOkter",						"DeskripsiObat"
                    };

                    foreach (var x in filter)
                    {
                        switch (x.Key)
                        {
                            case "NRM":
                                if (string.IsNullOrEmpty(x.Value)) break;
                                parameters.Add(new SqlParameter($"@NRM", $"{x.Value}"));
                                wheres.Add($"(NRM = @NRM)");
                                break;
                        }
                    }

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Obat
                        {s.ToWhere(wheres)}
                    ";

                    totalRowCount = await s.GetTotalRowConst(query, parameters);
                    datas = await s.GetDatas<Obat_Obat_ListModel>(query, _orderBy[orderBy], orderByType,  pageIndex, pageSize, parameters);
                }

                return Json(new
                {
                    Success = true,
                    Datas = datas,
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalCount = totalRowCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }

        [HttpGet]
        public async Task<JsonResult> Data_ObatDetail(string NoBukti)
        {
            try
            {
                var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                Obat_HeaderDetail result;

                using (var s = new MahasConnection(Config.GetConnectionString("EmrPuriBundaConnection")))
                {
                    result = await s.GetData<Obat_HeaderDetail>("SELECT * FROM Vw_Obat WHERE NoBukti = @NoBukti", new List<SqlParameter>()
                    {
                        new SqlParameter("@NoBukti", NoBukti)
                    });

                    var query = $@"
                        SELECT
                            *
                        FROM
                            Vw_Obat_Detail
                        WHERE NoBukti = @NoBukti
                    ";

                    result.Detail = await s.GetDatas<ObatDetail_ObatDetail_ListModel>(query, new List<SqlParameter>()
                    {
                        new SqlParameter("@NoBukti", NoBukti)
                    }) ?? new List<ObatDetail_ObatDetail_ListModel>();
                }

                return Json(new
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    ex.Message
                });
            }
        }
    }
}