using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("AspNetUsers")]	public class User_UserModel	{		[DbColumn]		public string Email { get; set; }		[DbKey]		[DbColumn]		public string Id { get; set; }		[DbColumn]		public string UserName { get; set; }	}
}