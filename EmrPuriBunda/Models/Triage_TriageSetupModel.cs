using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mTriage")]	public class Triage_TriageSetupModel	{		[DbKey(true)]		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string Triage { get; set; }	}
}