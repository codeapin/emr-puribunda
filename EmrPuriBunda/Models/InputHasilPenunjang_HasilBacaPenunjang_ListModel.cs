using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("Vw_HasilBacaPenunjang")]	public class InputHasilPenunjang_HasilBacaPenunjang_ListModel	{
        [DbColumn]
        public string NoBukti { get; set; }

        [DbColumn]
        public string HasilBaca { get; set; }

        [DbColumn]
        public string SectionID { get; set; }

        [DbColumn]
        public string NoReg { get; set; }

        [DbColumn]
        public bool? CriticalValue { get; set; }

        [DbColumn]
        public string VendorRujukanID { get; set; }

        [DbColumn]
        public DateTime? TanggalPeriksa { get; set; }

        [DbColumn]
        public string SectionAsalID { get; set; }

        [DbColumn]
        public string DokterID { get; set; }

        [DbColumn]
        public string PetugasID { get; set; }

        [DbColumn]
        public string Pemeriksaan { get; set; }

        public string NRM { get; set; }

        public string NamaPasien { get; set; }

        public string Alamat { get; set; }

        public string JenisKelamin { get; set; }

        public string JenisKerjasama { get; set; }

        public DateTime? TglLahir { get; set; }

        public string SectionID_Text { get; set; }

        public string VendorRujukanID_Text { get; set; }

        public string SectionAsalID_Text { get; set; }

        public string DokterID_Text { get; set; }

        public string PetugasID_Text { get; set; }
    }
}