using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
	[DbTable("Vw_trDokumenRekamMedisDetail")]	public class DokumenRekamMedis_Detail_ListModel	{		[DbColumn]		public string FileName { get; set; }

		[DbColumn]
		public string NoBukti { get; set; }

		[DbColumn]		public int? NoUrut { get; set; }
		[DbColumn]		public DateTime? TanggalDibuat { get; set; }		[DbColumn]		public string NoReg { get; set; }		[DbColumn]		public string NRM { get; set; }		[DbColumn]		public DateTime? TglReg { get; set; }		[DbColumn]		public int? JenisKerjasamaID { get; set; }		[DbColumn]		public string JenisKerjasama { get; set; }		[DbColumn]		public string SectionID { get; set; }		[DbColumn]		public string UserID { get; set; }		[DbColumn]		public bool? RawatJalan { get; set; }		[DbColumn]		public bool? RawatInap { get; set; }		[DbColumn]		public string Catatan { get; set; }		[DbColumn]		public string NamaPasien { get; set; }		[DbColumn]		public string JenisKelamin { get; set; }		[DbColumn]		public string Alamat { get; set; }		[DbColumn]		public DateTime? TglLahir { get; set; }

		[DbColumn]
		public bool? ArsipClaimBpjs { get; set; }

		public int? Tahun { get => TglReg.GetValueOrDefault().Year; }

		public string Info
		{
			get => $"{TglReg.GetValueOrDefault():yyyyMMdd}_{(RawatInap == true ? "RI" : "RJ")}_{JenisKerjasama}";
		}

		public string InfoTgl { get => $"{TglReg.GetValueOrDefault():yyyyMMdd}"; }


		public string UrlDokumen { get; set; }
    }

	public class DokumenRekamMedis_GroupTahun	{		public int? Tahun { get; set; }

        public List<DokumenRekamMedis_GroupInfo> GroupInfo { get; set; }
    }

	public class DokumenRekamMedis_GroupInfo	{
		public int? Tahun { get; set; }
		public string Info { get; set; }

		public string InfoTgl { get; set; }

		public List<DokumenRekamMedis_Detail_ListModel> Datas { get; set; }

		public DokumenRekamMedis_GroupClaim GroupClaim { get; set; }
	}

	public class DokumenRekamMedis_GroupClaim	{
		public string ClaimInfo { get; set; }

		public List<DokumenRekamMedis_Detail_ListModel> Datas { get; set; }
	}
}