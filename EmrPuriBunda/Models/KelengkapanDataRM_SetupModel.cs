﻿using Mahas.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmrPuriBunda.Models
{
    [DbTable("KelengkapanDataRM")]

    public class KelengkapanDataRM_SetupModel
    {
        [DbKey]
        [DbColumn]
        public string NoReg { get; set; }

        [DbColumn]
        public bool AsesmenDokterRJ { get; set; }

        [DbColumn]
        public bool AsesmenDokterRI { get; set; }

        [DbColumn]
        public bool AsesmenDokterUGD { get; set; }

        [DbColumn]
        public bool AsesmenPerawatRI { get; set; }

        [DbColumn]
        public bool AsesmenPerawatRJ { get; set; }

        [DbColumn]
        public bool AsesmenPerawatUGD { get; set; }

        [DbColumn]
        public bool CPPT { get; set; }

    }
}
