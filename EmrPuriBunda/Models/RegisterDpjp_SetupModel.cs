using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{	public class RegisterDpjp_SetupModel	{
        public string DokterId { get; set; }

		public string DokterId_Text { get; set; }

		public List<RegisterDpjp_RegisterDpjp_tabelRegisterDpjp_SetupModel> tabelRegisterDpjp { get; set; }	}	[DbTable("trRegisterDokterDPJPStatus")]	public class RegisterDpjp_RegisterDpjp_tabelRegisterDpjp_SetupModel	{		[DbKey]		[DbColumn]		public string NoReg { get; set; }		[DbKey]		[DbColumn]		public string DokterID { get; set; }		[DbKey]		[DbColumn]		public int Id_mStatusDPJP { get; set; }		public string DokterID_Text { get; set; }		public string Id_mStatusDPJP_Text { get; set; }	}
}