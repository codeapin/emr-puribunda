using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;
using System.Linq;

namespace EmrPuriBunda.Models
{
    [DbTable("Vw_Obat")]	public class Obat_Obat_ListModel	{		[DbColumn]		public string NoBukti { get; set; }		[DbColumn]		public string NoResep { get; set; }		[DbColumn]		public DateTime Tanggal { get; set; }		[DbColumn]		public string NamaDOkter { get; set; }		[DbColumn]		public string DeskripsiObat { get; set; }		[DbColumn]		public string NoReg { get; set; }		[DbColumn]		public string NRM { get; set; }		public string NamaSection { get; set; }

		public int? Puyer { get; set; }

		public string SatuanPuyer { get; set; }

		public int? Cito { get; set; }

		public string CatatanFarmasi { get; set; }		public int QtyPuyer { get; set; }

		public double? BeratBadan { get; set; }

		public string Keterangan { get; set; }	}

	public class Obat_HeaderDetail	{		public string NoBukti { get; set; }
		public string NoResep { get; set; }
		public DateTime Tanggal { get; set; }
		public string NamaDOkter { get; set; }
		public string DeskripsiObat { get; set; }
		public string NoReg { get; set; }

		public string NamaSection { get; set; }

		public int? Puyer { get; set; }

		public string SatuanPuyer { get; set; }

		public int? Cito { get; set; }

		public string CatatanFarmasi { get; set; }

		public double? BeratBadan { get; set; }

		public string Aturan1 { get => Detail?.FirstOrDefault()?.Dosis ?? ""; }

		public string Aturan2 { get => Detail?.FirstOrDefault()?.aturan2 ?? ""; }

		public List<ObatDetail_ObatDetail_ListModel> Detail { get; set; }
	}
}