using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    public class Resep_Resep_table1_LookUpModel	{		public string NamaBarang { get; set; }		public string NamaGenerik { get; set; }		public string Kode_Barang { get; set; }		public string Satuan_Stok { get; set; }		public bool? Aktif { get; set; }		public string Kelompok { get; set; }		public string SectionID { get; set; }		public string Nama_Kategori { get; set; }		public int? Qty_Stok { get; set; }		public bool? BarangLokasiNew_Aktif { get; set; }		public int? Barang_ID { get; set; }		public string Nama_Sub_Kategori { get; set; }		public decimal? Harga_Jual { get; set; }	}
}