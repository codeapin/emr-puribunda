using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("Vw_mTemplateAsesmen")]	public class TemplateAsesmen_ListModel	{		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string Kode { get; set; }		[DbColumn]		public int? JenisID { get; set; }		[DbColumn]		public string SectionID { get; set; }		[DbColumn]		public string SpesialisasiID { get; set; }		[DbColumn]		public string DokterID { get; set; }		[DbColumn]		public string Judul { get; set; }		[DbColumn]		public string TempData { get; set; }		[DbColumn]		public bool? Active { get; set; }		[DbColumn]		public string SpesialisasiID_Text { get; set; }		[DbColumn]		public string JenisID_Text { get; set; }		[DbColumn]		public string DokterID_Text { get; set; }		[DbColumn]		public string SectionID_Text { get; set; }	}
}