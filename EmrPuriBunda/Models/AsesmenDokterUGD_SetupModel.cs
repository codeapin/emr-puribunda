using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;
using FluentValidation;
using EmrPuriBunda.Mahas.Helpers;

namespace EmrPuriBunda.Models
{
    [DbTable("AsesmenDokterUGD")]	public class AsesmenDokterUGD_SetupModel	{		[DbKey]		[DbColumn]		public string NoReg { get; set; }		[DbColumn]
		public string NRM { get; set; }		[DbColumn]		public string LokasiMekanisneTrauma { get; set; }		[DbColumn]		public byte? Id_mJenisKunjungan { get; set; }		[DbColumn]		public DateTime? WaktuDatang { get; set; }		[DbColumn]		public DateTime? WaktuPelayanan { get; set; }		[DbColumn]		public byte? Id_mCaraDatang { get; set; }		[DbColumn]		public byte? Id_mTriage { get; set; }		[DbColumn]		public byte? Id_mKeadaanSaatDatang { get; set; }		[DbColumn]		public byte? Id_mRujukan { get; set; }		[DbColumn]
        public string RujukanCatatan { get; set; }		[DbColumn]		public bool? Autoanamenis { get; set; }		[DbColumn]		public bool? Alloanamnesis { get; set; }		[DbColumn]		public string Anamnesis { get; set; }		[DbColumn]		public byte? Id_mAlasanKunjungan { get; set; }		[DbColumn]		public byte? Id_mHubunganPasien { get; set; }		[DbColumn]
        public string RiwayatAlergi { get; set; }		[DbColumn]		public string RiwayatPenyakitDahulu { get; set; }		[DbColumn]		public string RiwayatPengobatan { get; set; }		[DbColumn]		public string PemeriksaanFisik { get; set; }		[DbColumn]		public string PemeriksaanPenunjang { get; set; }		[DbColumn]		public string DiagnosaUtama { get; set; }		[DbColumn]		public string DiagnosaUtamaKeterangan { get; set; }		[DbColumn]		public string Planning { get; set; }		[DbColumn]		public string Instruksi { get; set; }

		[DbColumn(isImage: true)]		public byte[] BodyDiagram { get; set; }				public string BodyDiagramView { get; set; }		[DbColumn]		public string Triage_Keterangan { get; set; }		[DbColumn]		public string KeadaanSaatDatangKeterangan { get; set; }

		[DbColumn]
		public bool? Hamil { get; set; }

		[DbColumn]
		public bool? DOA { get; set; }
		[DbColumn]
		public bool? Trauma { get; set; }		[DbColumn]		public string UserId { get; set; }		[DbColumn]		public bool? IsiDenganBenar { get; set; }

		[DbColumn]		public string SectionId { get; set; }		[DbColumn]		public string DokterId { get; set; }		[DbColumn]		public bool? SimpanSementara { get; set; }

		[DbColumn]
		public bool? Tulis { get; set; }


		[DbColumn]
		public bool? Baca { get; set; }


		[DbColumn]
		public bool? ButuhKonfirmasi { get; set; }
		
		[DbColumn]

		public string Konfirmasi_Dpjp { get; set; }
		public string Konfirmasi_Dpjp_Text { get; set; }

		public int? Id_Cppt { get; set; }

		public string Id_mAlasanKunjungan_Text { get; set; }		public string Id_mKeadaanSaatDatang_Text { get; set; }		public string Id_mCaraDatang_Text { get; set; }		public string Id_mStatusPsikologis_Text { get; set; }		public string Id_mHubunganPasien_Text { get; set; }		public string Id_mTriage_Text { get; set; }		public string Id_mJenisKunjungan_Text { get; set; }		public string Id_mRujukan_Text { get; set; }		public string DiagnosisSekunder { get; set; }		public string DiagnosaUtama_Text { get; set; }		public List<AsesmenDokterUGD_AsesmenDokterUGDtable1SetupModel> table1 { get; set; }	}	[DbTable("AsesmenDokterUGD_DiagnosaSekunder")]	public class AsesmenDokterUGD_AsesmenDokterUGDtable1SetupModel	{		[DbKey]		[DbColumn]		public string NoReg { get; set; }		[DbKey]		[DbColumn]		public string KodeICD { get; set; }		[DbColumn]		public int NoUrut { get; set; }		public string KodeICD_Text { get; set; }	}

	public class AsesmenDokterUGD_SetupModelValidator : AbstractValidator<AsesmenDokterUGD_SetupModel>
	{
		public AsesmenDokterUGD_SetupModelValidator()
		{
			RuleFor(x => x.IsiDenganBenar).Must(x => x == true).WithCustomMessage("harus dicentang.", "Saya telah mengisi asesmen dengan benar");
		}
	}
}