using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;
using FluentValidation;

namespace EmrPuriBunda.Models
{
    [DbTable("trOrderPenunjang")]	public class OrderPenunjang_OrderPenunjang_SetupModel	{		[DbKey]		[DbColumn]		public string NoBukti { get; set; }		[DbColumn]		public string Noreg { get; set; }		[DbColumn]		public string NamaPasien { get; set; }		[DbColumn]		public bool? Hasil_Cito { get; set; }		[DbColumn]		public bool? Hasil_Rutin { get; set; }		[DbColumn]		public string KodeICD { get; set; }		[DbColumn]		public string DiagnosaIndikasi { get; set; }		[DbColumn]		public string DokterID { get; set; }		[DbColumn]		public string NamaVendor { get; set; }		[DbColumn]		public string AlamatVendor { get; set; }		[DbColumn]		public string TlpVendor { get; set; }		[DbColumn]		public DateTime? DiterimaTgl { get; set; }		[DbColumn]		public DateTime? JamSampling { get; set; }		[DbColumn]		public DateTime? JamBilling { get; set; }		[DbColumn]		public string KodePetugas { get; set; }		[DbColumn]		public string NamaPetugas { get; set; }		[DbColumn]		public string PemeriksaanTambahan { get; set; }		[DbColumn]		public bool? Realisasi { get; set; }

		[DbColumn]
        public string UserId { get; set; }

		[DbColumn]
        public string SectionID { get; set; }

		[DbColumn]
        public DateTime Tanggal { get; set; }

		[DbColumn]
		public string SectionAsalID { get; set; }

		public string SectionName { get; set; }

		public List<JasaKelompok> JasaList { get; set; }		public List<OrderPenunjang_DetailManual_SetupModel> tb_orderPenunjangDetailManual { get; set; }	}	[DbTable("trOrderPenunjangDetailManual")]	public class OrderPenunjang_DetailManual_SetupModel	{		[DbKey]		[DbColumn]		public string NoBukti { get; set; }		[DbColumn]		public int? NoUrut { get; set; }		[DbKey]		[DbColumn]		public string JasaID { get; set; }		[DbColumn]		public int Qty { get; set; }		[DbColumn]		public string Keterangan { get; set; }		public string JasaName { get; set; }		public string Alias { get; set; }	}

	public class JasaKelompok
    {
        public string JasaID { get; set; }

        public int KelompokID { get; set; }
    }


    #region Validator

    public class OrderPenunjangValidator : AbstractValidator<OrderPenunjang_OrderPenunjang_SetupModel>
    {
        public OrderPenunjangValidator()
        {
			RuleFor(x => x.SectionID).NotEmpty();
			RuleFor(x => x.DokterID).NotEmpty();
			RuleFor(x => x.NoBukti).NotEmpty();
			RuleFor(x => x.DiagnosaIndikasi).NotEmpty();
		}
	}

    #endregion
}