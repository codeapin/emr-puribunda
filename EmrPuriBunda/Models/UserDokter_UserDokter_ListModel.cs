using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("Vw_UserDokter")]	public class UserDokter_UserDokter_ListModel	{		[DbColumn]		public string UserId { get; set; }		[DbColumn]		public string UserName { get; set; }		[DbColumn]		public string DokterId { get; set; }		[DbColumn]		public string Nama_Supplier { get; set; }	}
}