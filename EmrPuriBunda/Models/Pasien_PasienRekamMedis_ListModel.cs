using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mPasien")]	public class Pasien_PasienRekamMedis_ListModel	{		[DbColumn]		public string NRM { get; set; }		[DbColumn]		public string NamaPasien { get; set; }		[DbColumn]		public string NoIdentitas { get; set; }		[DbColumn]		public string JenisKelamin { get; set; }		[DbColumn]		public DateTime? TglLahir { get; set; }		[DbColumn]		public bool TglLahirDiketahui { get; set; }		[DbColumn]		public short? UmurSaatInput { get; set; }		[DbColumn]		public string Pekerjaan { get; set; }		[DbColumn]		public string Alamat { get; set; }		[DbColumn]		public short? PropinsiID { get; set; }		[DbColumn]		public string KabupatenID { get; set; }		[DbColumn]		public string KecamatanID { get; set; }		[DbColumn]		public string DesaID { get; set; }		[DbColumn]		public string BanjarID { get; set; }		[DbColumn]		public string Phone { get; set; }		[DbColumn]		public string Email { get; set; }		[DbColumn]		public string JenisPasien { get; set; }		[DbColumn]		public int JenisKerjasamaID { get; set; }		[DbColumn]		public bool AnggotaBaru { get; set; }		[DbColumn]		public decimal? CustomerKerjasamaID { get; set; }		[DbColumn]		public string CompanyID { get; set; }		[DbColumn]		public string NoKartu { get; set; }		[DbColumn]		public string Klp { get; set; }		[DbColumn]		public string JabatanDiPerusahaan { get; set; }		[DbColumn]		public bool PasienLoyal { get; set; }		[DbColumn]		public int TotalKunjunganRawatInap { get; set; }		[DbColumn]		public int TotalKunjunganRawatJalan { get; set; }		[DbColumn]		public int KunjunganRJ_TahunIni { get; set; }		[DbColumn]		public int KunjunganRI_TahunIni { get; set; }		[DbColumn]		public string EtnisID { get; set; }		[DbColumn]		public string NationalityID { get; set; }		[DbColumn]		public bool PasienVVIP { get; set; }		[DbColumn]		public bool PasienKTP { get; set; }		[DbColumn]		public DateTime? TglInput { get; set; }		[DbColumn]		public short? UserID { get; set; }		[DbColumn]		public string CaraDatangPertama { get; set; }		[DbColumn]		public string DokterID_ReferensiPertama { get; set; }		[DbColumn]		public bool SedangDirawat { get; set; }		[DbColumn]		public int? JmlKunjunganHD { get; set; }		[DbColumn]		public int JmlKunjunganDB { get; set; }		[DbColumn]		public bool KomunitasDB { get; set; }		[DbColumn]		public DateTime? TglMulaiKomunitasDB { get; set; }		[DbColumn]		public int? JmlRIThnIni { get; set; }		[DbColumn]		public int? JmlRIOpnameIni { get; set; }		[DbColumn]		public DateTime? LastDateRI { get; set; }		[DbColumn]		public string KodePos { get; set; }		[DbColumn]		public DateTime? TglRegKasusKecelakaanBaru { get; set; }		[DbColumn]		public string NoRegKecelakaanBaru { get; set; }		[DbColumn]		public bool Aktive_Keanggotaan { get; set; }		[DbColumn]		public string Agama { get; set; }		[DbColumn]		public string NoANggotaE { get; set; }		[DbColumn]		public string NamaAnggotaE { get; set; }		[DbColumn]		public string GenderAnggotaE { get; set; }		[DbColumn]		public DateTime? TglTidakAktif { get; set; }		[DbColumn]		public string TipePasienAsal { get; set; }		[DbColumn]		public string NoKartuAsal { get; set; }		[DbColumn]		public string NamaPerusahaanAsal { get; set; }		[DbColumn]		public bool? PenanggungIsPasien { get; set; }		[DbColumn]		public string PenanggungNRM { get; set; }		[DbColumn]		public string PenanggungNama { get; set; }		[DbColumn]		public string PenanggungAlamat { get; set; }		[DbColumn]		public string PenanggungPhone { get; set; }		[DbColumn]		public string PenanggungKTP { get; set; }		[DbColumn]		public string PenanggungHubungan { get; set; }		[DbColumn]		public string PenanggungPekerjaan { get; set; }		[DbColumn]		public bool? Aktif { get; set; }		[DbColumn]		public bool? PasienBlackList { get; set; }		[DbColumn]		public string NamaIbuKandung { get; set; }		[DbColumn]		public bool? NonPBI { get; set; }		[DbColumn]		public string KdKelas { get; set; }		[DbColumn]		public string TempatLahir { get; set; }		[DbColumn]		public DateTime? JamLahir { get; set; }		[DbColumn]		public double? BBLahir { get; set; }		[DbColumn]		public double? PBLahir { get; set; }		[DbColumn]		public bool? LahirNormal { get; set; }		[DbColumn]		public bool? LahirSC { get; set; }		[DbColumn]		public bool? Prematur { get; set; }		[DbColumn]		public string NamaAlias { get; set; }		[DbColumn]		public int? PrintKartu { get; set; }		[DbColumn]		public string Pendidikan { get; set; }		[DbColumn]		public string HambatanBerkomunikasi { get; set; }		[DbColumn]		public string Kode_Regional { get; set; }

        public string JenisKerjasama { get; set; }

		public string JenisKerjasamaID_Text { get => JenisKerjasama; }

		public string UmurTahun
		{
			get
			{
				if (TglLahir == null) return "? tahun";

				var umurth = Umur.IndexOf("tahun");

				return Umur.Substring(0, 0 + umurth) + "tahun";
			}
		}

		public string Umur
		{
			get
			{
				if (TglLahir == null) return "";

				var dob = TglLahir.GetValueOrDefault();

				var today = DateTime.Today;

				// Calculate the age.
				int months = today.Month - dob.Month;

				int years = today.Year - dob.Year;

				if (today.Day < dob.Day)
				{
					months--;
				}

				if (months < 0)
				{
					years--;
					months += 12;
				}

				int days = (today - dob.AddMonths((years * 12) + months)).Days;

				return $"{years} tahun, {months} bulan, {days} hari";
			}
		}	}
}