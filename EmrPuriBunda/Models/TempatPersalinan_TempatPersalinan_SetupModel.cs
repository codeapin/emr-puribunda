using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mTempatPersalinan")]	public class TempatPersalinan_TempatPersalinan_SetupModel	{		[DbKey(true)]		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string TempatPersalinan { get; set; }		[DbColumn]		public bool? Aktif { get; set; }	}
}