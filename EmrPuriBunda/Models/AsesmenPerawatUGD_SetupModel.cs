using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;
using FluentValidation;
using EmrPuriBunda.Mahas.Helpers;

namespace EmrPuriBunda.Models
{
    [DbTable("AsesmenPerawatUGD")]	public class AsesmenPerawatUGD_SetupModel	{		[DbKey]		[DbColumn]		public string NoReg { get; set; }

		[DbColumn]
		public string NRM { get; set; }

        [DbColumn]		public string LokasiMekanisneTrauma { get; set; }		[DbColumn]		public byte? Id_mJenisKunjungan { get; set; }		[DbColumn]		public DateTime? WaktuDatang { get; set; }		[DbColumn]		public DateTime? WaktuPelayanan { get; set; }		[DbColumn]		public byte? Id_mCaraDatang { get; set; }		[DbColumn]		public byte? Id_mTriage { get; set; }		[DbColumn]		public byte? Id_mKeadaanSaatDatang { get; set; }				[DbColumn]		public string KeadaanSaatDatangKeterangan { get; set; }		[DbColumn]		public byte? Id_mRujukan { get; set; }		[DbColumn]		public string Rujukan_Keterangan { get; set; }		[DbColumn]		public bool? Autoanamnesis { get; set; }		[DbColumn]		public bool? Alloanamnesis { get; set; }		[DbColumn]		public string Anamnesis { get; set; }		[DbColumn]		public byte? Id_mAlasanKunjungan { get; set; }		[DbColumn]		public byte? Id_mHubunganPasien { get; set; }


		[DbColumn]
		public bool? Hamil { get; set; }

		[DbColumn]		public bool? DOA { get; set; }

		[DbColumn]		public bool? Trauma { get; set; }

		[DbColumn]		public string RiwayatAlergi { get; set; }		[DbColumn]		public string RiwayatPenyakitDahulu { get; set; }		[DbColumn]		public string RiwayatPengobatan { get; set; }		[DbColumn]		public string SkriningFungsional { get; set; }		[DbColumn]		public string SkriningFungsionalCatatan { get; set; }		[DbColumn]		public string SkriningResikoJatuh { get; set; }		[DbColumn]		public string SkriningResikoJatuhCatatan { get; set; }		[DbColumn]		public string SkriningResikoNutrisi { get; set; }		[DbColumn]		public string SkriningResikoNutrisiCatatan { get; set; }		[DbColumn]		public string SkriningNyeri { get; set; }		[DbColumn]		public string SkriningNyeriCatatan { get; set; }		[DbColumn]		public bool? ButuhEdukasi { get; set; }		[DbColumn]		public string ButuhEdukasiCatatan { get; set; }		[DbColumn]		public string HubunganDenganAnggotaKeluarga { get; set; }		[DbColumn]		public bool? KecendrunganBunuhDiri { get; set; }		[DbColumn]		public bool? MembutuhkanPrivasiTambahan { get; set; }		[DbColumn]		public bool? MasalahEkonomi { get; set; }		[DbColumn]		public byte? Id_mStatusPsikologi { get; set; }		[DbColumn]		public string Objektif_PemeriksaanFisik { get; set; }		[DbColumn]		public string Asesmen_MasalahKeperawatan { get; set; }		[DbColumn]		public string Planning { get; set; }		[DbColumn]		public string Instruksi { get; set; }		[DbColumn]		public string Triage_Keterangan { get; set; }

		[DbColumn]		public bool? IsiDenganBenar { get; set; }		[DbColumn]		public string UserId { get; set; }

		[DbColumn]		public string SectionId { get; set; }		[DbColumn]		public string DokterId { get; set; }		[DbColumn]		public bool? SimpanSementara { get; set; }		public int? Id_Cppt { get; set; }		public string Id_mAlasanKunjungan_Text { get; set; }		public string Id_mKeadaanSaatDatang_Text { get; set; }		public string Id_mStatusPsikologi_Text { get; set; }		public string Id_mCaraDatang_Text { get; set; }		public string Id_mHubunganPasien_Text { get; set; }		public string Id_mTriage_Text { get; set; }		public string Id_mJenisKunjungan_Text { get; set; }		public string SkriningFungsional_Text { get; set; }		public string SkriningResikoJatuh_Text { get; set; }		public string SkriningResikoNutrisi_Text { get; set; }		public string SkriningNyeri_Text { get; set; }		public string Id_mRujukan_Text { get; set; }	}

	public class AsesmenPerawatUGD_SetupModelValidator : AbstractValidator<AsesmenPerawatUGD_SetupModel>
	{
		public AsesmenPerawatUGD_SetupModelValidator()
		{
			RuleFor(x => x.IsiDenganBenar).Must(x => x == true).WithCustomMessage("Saya telah mengisi asesmen dengan benar harus dicentang.", "");
		}
	}
}