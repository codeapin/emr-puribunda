using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("trRiwayatPersalinan")]	public class RiwayatPersalinan_ListModel	{
        [DbKey(true)]
        [DbColumn]
        public int? Id { get; set; }

        [DbColumn]
        public string NRM { get; set; }

        [DbColumn]
        public int? TahunPersalinan { get; set; }

        [DbColumn]
        public string PenyulitKehamilan { get; set; }

        [DbColumn]
        public string BBL { get; set; }

        [DbColumn]
        public string JenisKelamin { get; set; }

        [DbColumn]
        public string Keterangan { get; set; }

        [DbColumn]
        public DateTime? DibuatTanggal { get; set; }

        [DbColumn]
        public string UserId { get; set; }

        [DbColumn]
        public string UsiaKehamilan { get; set; }

        [DbColumn]
        public int? Id_mPenolongPersalinan { get; set; }
        [DbColumn]
        public int? Id_mCaraPersalinan { get; set; }
        [DbColumn]
        public int? Id_mTempatPersalinan { get; set; }


        public string Id_mPenolongPersalinan_Text { get; set; }
        public string Id_mCaraPersalinan_Text { get; set; }
        public string Id_mTempatPersalinan_Text { get; set; }
    }
}