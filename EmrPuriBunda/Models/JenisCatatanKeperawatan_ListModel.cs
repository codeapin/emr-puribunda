using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mJenisCatatanKeperawatan")]	public class JenisCatatanKeperawatan_ListModel	{		[DbKey(true)]		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string Jenis { get; set; }	}
}