using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mSkriningKelompok")]	public class SkriningKelompok_SkriningKelompokModel	{		[DbKey(true)]		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string SkriningKelompok { get; set; }	}
}