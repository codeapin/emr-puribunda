﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmrPuriBunda.Models
{
    public class mSectionLogin
    {
        public string GroupPelayanan { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string EResepSectionID { get; set; }
        public string EResepSectionName { get; set; }
        public int? NoUrut { get; set; }

    }
}
