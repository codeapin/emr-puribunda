using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
	[DbTable("EMR_VIEW_LABORATORIUM_DETAIL")]	public class HasilBacaLabDetail_HasilBacaLabDetailModel	{		[DbColumn]		public string KategoriTestNama { get; set; }		[DbColumn]		public string NamaTest { get; set; }		[DbColumn]		public string Nilai { get; set; }		[DbColumn]		public string NilaiRujukan { get; set; }		[DbColumn]		public string Satuan { get; set; }	}

	public class HasilBacaLabDetail_SetupModel	{		public InputHasilPenunjang_KomentarPenunjang_SetupModel ReviewHasil { get; set; }		public List<HasilBacaLabDetail_GroupKategori> GroupKategori { get; set; }	}

	public class HasilBacaLabDetail_GroupKategori
    {
		public string Kategori { get; set; }
		public List<HasilBacaLabDetail_HasilBacaLabDetailModel> Hasil { get; set; }
	}
}