using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("Vw_mSkrining")]	public class Skrining_SkriningModel	{		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string Id_mSkriningKelompok_Text { get; set; }		[DbColumn]		public string Skrining { get; set; }	}
}