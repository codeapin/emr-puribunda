using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using EmrPuriBunda.Mahas.Helpers;

namespace EmrPuriBunda.Models
{
    [DbTable("SIMtrResep")]	public class Resep_Resep_SetupModel	{		[DbKey]		[DbColumn]		public string NoResep { get; set; }		[DbColumn]		public string NoRegistrasi { get; set; }		[DbColumn]		public string SectionID { get; set; }		[DbColumn]		public DateTime Tanggal { get; set; }		[DbColumn]		public DateTime Jam { get; set; }		[DbColumn]		public string DokterID { get; set; }		[DbColumn]		public decimal Jumlah { get; set; }		[DbColumn]		public decimal KomisiDokter { get; set; }		[DbColumn]		public bool Cyto { get; set; }		[DbColumn]		public string NoBukti { get; set; }		[DbColumn]		public string Farmasi_SectionID { get; set; }		[DbColumn]		public int User_ID { get; set; }		[DbColumn]		public string JenisKerjasamaID { get; set; }		[DbColumn]		public string CompanyID { get; set; }		[DbColumn]		public string NRM { get; set; }		[DbColumn]		public string NoKartu { get; set; }		[DbColumn]		public string KelasID { get; set; }		[DbColumn]		public bool Realisasi { get; set; }		[DbColumn]		public bool KTP { get; set; }		[DbColumn]		public string KerjasamaID { get; set; }		[DbColumn]		public string PerusahaanID { get; set; }		[DbColumn]		public bool RawatInap { get; set; }		[DbColumn]		public bool Batal { get; set; }		[DbColumn]		public bool? Paket { get; set; }		[DbColumn]		public string KodePaket { get; set; }		[DbColumn]		public bool? AmprahanRutin { get; set; }		[DbColumn]		public bool? IncludeJasa { get; set; }		[DbColumn]		public int? NoAntri { get; set; }		[DbColumn]		public string UserNameInput { get; set; }		[DbColumn]		public string Keterangan { get; set; }		[DbColumn]		public bool? Puyer { get; set; }		[DbColumn]		public int? QtyPuyer { get; set; }		[DbColumn]		public string SatuanPuyer { get; set; }		public string SatuanPuyer_Text { get; set; }		[DbColumn]		public int? CustomerKerjasamaID { get; set; }		[DbColumn]		public bool? RasioObat_ada { get; set; }		[DbColumn]		public bool? Rasio_KelebihanDibayarPasien { get; set; }		[DbColumn]		public bool? RasioUmum_Alert { get; set; }		[DbColumn]		public bool? RasioUmum_Blok { get; set; }		[DbColumn]		public bool? RasioSpesialis_Alert { get; set; }		[DbColumn]		public bool? RasioSPesialis_Blok { get; set; }		[DbColumn]		public bool? RasioSub_Alert { get; set; }		[DbColumn]		public bool? RasioSub_Blok { get; set; }		[DbColumn]		public decimal? RasioUmum_nilai { get; set; }		[DbColumn]		public decimal? RasioSpesialis_Nilai { get; set; }		[DbColumn]		public decimal? RasioSub_Nilai { get; set; }		[DbColumn]		public double? BeratBadan { get; set; }		[DbColumn]		public bool? SedangProses { get; set; }		[DbColumn]		public string SedangProsesPC { get; set; }		public string SectionAwalName { get; set; }		public string Farmasi_SectionID_Text { get; set; }		public string NamaPaket { get; set; }		public string DokterID_Text { get; set; }

        public string HeaderAturan1 { get; set; }

		public string HeaderAturan2 { get; set; }

		public List<Resep_Resep_table1_SetupModel> table1 { get; set; }	}	public class Resep_Resep_GetData
    {
		public string NoResep { get; set; }
		public string NoRegistrasi { get; set; }
		public string SectionID { get; set; }
		public DateTime Tanggal { get; set; }
		public DateTime Jam { get; set; }
		public string DokterID { get; set; }
		public decimal Jumlah { get; set; }
		public bool Cyto { get; set; }
		public string NoBukti { get; set; }
		public string Farmasi_SectionID { get; set; }
		public bool? Paket { get; set; }
		public string KodePaket { get; set; }
		public string Keterangan { get; set; }
		public bool? Puyer { get; set; }
		public int? QtyPuyer { get; set; }
		public string SatuanPuyer { get; set; }
		public string SatuanPuyer_Text { get=> SatuanPuyer; }
		public string SectionAwalName { get; set; }
		public string Farmasi_SectionID_Text { get; set; }
		public string NamaPaket { get; set; }
		public string DokterID_Text { get; set; }
		public double? BeratBadan { get; set; }

		public string BeratBadanKoma { get; set; }

		public string HeaderAturan1 { get; set; }
		public string HeaderAturan2 { get; set; }

		public List<Resep_Resep_table1_GetData> table1 { get; set; }
	}	[DbTable("SIMtrResepDetail")]	public class Resep_Resep_table1_SetupModel	{		[DbKey]		[DbColumn]		public string NoResep { get; set; }		[DbKey]		[DbColumn]		public int Barang_ID { get; set; }		[DbColumn]		public string Satuan { get; set; }		[DbColumn]		public double Qty { get; set; }		[DbColumn]		public decimal Harga_Satuan { get; set; }		[DbColumn]		public double Disc_Persen { get; set; }		[DbColumn]		public double Stok { get; set; }		[DbColumn]		public decimal? KomisiDokter { get; set; }		[DbColumn]		public decimal THT { get; set; }		[DbColumn]		public bool Racik { get; set; }		[DbColumn]		public decimal Plafon { get; set; }		[DbColumn]		public decimal KelebihanPLafon { get; set; }		[DbColumn]		public string KelasID { get; set; }		[DbColumn]		public bool? KTP { get; set; }		[DbColumn]		public string JenisKerjasamaID { get; set; }		[DbColumn]		public string PerusahaanID { get; set; }		[DbColumn]		public string DokterID { get; set; }		[DbColumn]		public string SectionID { get; set; }		[DbColumn]		public double PPN { get; set; }		[DbColumn]		public int DosisID { get; set; }		[DbColumn]		public short? JenisBarangID { get; set; }		[DbColumn]		public decimal Embalase { get; set; }		[DbColumn]		public decimal JasaRacik { get; set; }		[DbColumn]		public string Dosis { get; set; }		[DbColumn]		public double? QtyRacik { get; set; }		[DbColumn]		public int? QtyHasilRacik { get; set; }		[DbColumn]		public bool? TermasukPaket { get; set; }		public decimal NomorUrut { get; set; }		[DbColumn]		public string KetDosis { get; set; }		[DbColumn]		public string Dosis2 { get; set; }		public string Nama_Barang { get; set; }	}
	public class Resep_Resep_table1_GetData
	{
		public string NoResep {get; set;}
		public int Barang_ID   {get; set;}
		public string Satuan {get; set;}
		public double Qty {get; set;}
		public decimal Harga_Satuan {get; set;}
		public double Stok {get; set;}
		public int DosisID {get; set;}
		public short? JenisBarangID {get; set;}
		public string Dosis {get; set;}
		public decimal NomorUrut {get; set;}
		public string KetDosis {get; set;}
		public string Dosis2 {get; set;}
		public string Nama_Barang {get; set;}

	}

    #region Validator
	public class PostResepValidator : AbstractValidator<Resep_Resep_GetData>
    {
        public PostResepValidator()
        {
			RuleFor(x => x.NoResep).HarusDiisi();
			RuleFor(x => x.DokterID).HarusDiisi("Dokter");
			RuleFor(x => x.Tanggal).HarusDiisi();
			RuleFor(x => x.Farmasi_SectionID).HarusDiisi("Farmasi");
			//RuleFor(x => x.BeratBadan).HarusDiisi("Berat Badan");

			When(x => x.Puyer == true, () =>
			{
				RuleFor(x => x.Keterangan).HarusDiisi("Nama Racikan");
				RuleFor(x => x.SatuanPuyer).HarusDiisi("Bentuk sediaan");
				RuleFor(x => x.QtyPuyer ?? 0).LebihBesarDari(0, "Jumlah Racik");
				RuleFor(x => x.HeaderAturan1).HarusDiisi("Aturan 1");
				//RuleFor(x => x.HeaderAturan1).HarusDiisi("Aturan 2");

				RuleForEach(x => x.table1).ChildRules(datas =>
				{
					datas.RuleFor(x => x.Barang_ID).HarusDiisi("Obat");
				});
			});

			When(x => x.Puyer != true, () =>
            {
				RuleForEach(x => x.table1).ChildRules(datas =>
				{
					datas.RuleFor(x => x.Barang_ID).HarusDiisi("Obat");
					datas.RuleFor(x => x.Qty).LebihBesarDari(0, "Qty Obat");
					datas.RuleFor(x => x.Dosis).HarusDiisi("Aturan 1");
					//datas.RuleFor(x => x.Dosis2).HarusDiisi("Aturan 2");
				});
            });
		}
    }

	public class PostResepDetailValidator : AbstractValidator<Resep_Resep_table1_GetData>
	{
		public PostResepDetailValidator()
		{
			
		}
	}

	#endregion

}