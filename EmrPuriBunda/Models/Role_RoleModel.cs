using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("AspNetRoles")]	public class Role_RoleModel	{		[DbKey]		[DbColumn]		public string Id { get; set; }		[DbColumn]		public string Name { get; set; }	}
}