using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mKeadaanSaatDatang")]	public class KeadaanSaatDatang_KeadaanSaatDatangSetupModel	{		[DbKey(true)]		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string KeadaanSaatDatang { get; set; }	}
}