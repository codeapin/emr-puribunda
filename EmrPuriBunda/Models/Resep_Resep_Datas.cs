using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;
using System.Linq;

namespace EmrPuriBunda.Models
{
    [DbTable("EResep_HeaderDetail")]	public class Resep_Resep_Datas	{		[DbColumn]		public string NoRegistrasi { get; set; }		[DbColumn]		public string NoResep { get; set; }		[DbColumn]		public DateTime Tanggal { get; set; }		[DbColumn]		public DateTime Jam { get; set; }		[DbColumn]		public string DokterID { get; set; }		[DbColumn]		public string NamaDokter { get; set; }		[DbColumn]		public string Farmasi_SectionID { get; set; }		[DbColumn]		public string SectionFarmasi { get; set; }		[DbColumn]		public string KodePaket { get; set; }		[DbColumn]		public string NamaPaket { get; set; }		[DbColumn]		public bool Cyto { get; set; }		[DbColumn]		public bool? Puyer { get; set; }		[DbColumn]		public int? QtyPuyer { get; set; }		[DbColumn]		public string SatuanPuyer { get; set; }		[DbColumn]		public string Keterangan { get; set; }		[DbColumn]		public bool? Realisasi { get; set; }		[DbColumn]		public bool? SedangProses { get; set; }
		[DbColumn]		public string SedangProsesNamaUser { get; set; }		[DbColumn]		public string Nama_Barang { get; set; }		[DbColumn]		public string Satuan { get; set; }		[DbColumn]		public string Dosis { get; set; }		[DbColumn]		public string Dosis2 { get; set; }		[DbColumn]		public double Qty { get; set; }		[DbColumn]		public double Stok { get; set; }		[DbColumn]		public string KetDosis { get; set; }		[DbColumn]		public double? BeratBadan { get; set; }		[DbColumn]		public bool? Batal { get; set; }	}

	public class Resep_Resep_Header	{		public string NoRegistrasi { get; set; }		public string NoResep { get; set; }		public DateTime Tanggal { get; set; }		public DateTime Jam { get; set; }		public string DokterID { get; set; }		public string NamaDokter { get; set; }		public string Farmasi_SectionID { get; set; }		public string SectionFarmasi { get; set; }		public string KodePaket { get; set; }		public string NamaPaket { get; set; }		public bool Cyto { get; set; }		public bool? Puyer { get; set; }		public int? QtyPuyer { get; set; }		public string SatuanPuyer { get; set; }		public string Keterangan { get; set; }				public bool? Realisasi { get; set; }		public bool? SedangProses { get; set; }		public string SedangProsesNamaUser { get; set; }

		public string BeratBadan { get; set; }		public bool? Batal { get; set; }		public string HeaderAturan1
		{
			get
			{
				return ResepDetail?.FirstOrDefault()?.Dosis ?? "";
			}
		}
		public string HeaderAturan2
		{
			get
			{
				return ResepDetail?.FirstOrDefault()?.Dosis2 ?? "";
			}
		}		public List<Resep_Resep_Detail> ResepDetail { get; set; }	}

	public class Resep_Resep_Detail
    {
        public Resep_Resep_Detail(Resep_Resep_Datas model)
        {
			Nama_Barang = model.Nama_Barang;
			Satuan = model.Satuan;
			Dosis = model.Dosis;
			Dosis2 = model.Dosis2;
			Qty = model.Qty;
			Stok = model.Stok;
			KetDosis = model.KetDosis;
		}

		public Resep_Resep_Detail()
        {

        }		public string Nama_Barang { get; set; }		public string Satuan { get; set; }		public string Dosis { get; set; }		public string Dosis2 { get; set; }		public double Qty { get; set; }		public double Stok { get; set; }		public string KetDosis { get; set; }
	}
}