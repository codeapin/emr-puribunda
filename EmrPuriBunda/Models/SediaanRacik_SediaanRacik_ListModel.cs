using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mSediaanRacik")]	public class SediaanRacik_SediaanRacik_ListModel	{		[DbKey(true)]		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string NamaRacikan { get; set; }	}
}