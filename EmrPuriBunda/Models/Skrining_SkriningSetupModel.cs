using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mSkrining")]	public class Skrining_SkriningSetupModel	{		[DbKey(true)]		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string Skrining { get; set; }		[DbColumn]		public int? Id_mSkriningKelompok { get; set; }		public string Id_mSkriningKelompok_Text { get; set; }	}
}