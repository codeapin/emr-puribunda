using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mOrderPenunjang_Kelompok")]	public class OrderPenunjangKelompok_SetupModel	{		[DbKey(true)]		[DbColumn]		public int ID { get; set; }		[DbColumn]		public int JenisID { get; set; }		[DbColumn]		public int? Nomor { get; set; }		[DbColumn]		public string Nama { get; set; }		public string JenisID_Text { get; set; }		public List<OrderPenunjangKelompok_tableJasa_SetupModel> tableJasa { get; set; }	}	[DbTable("mOrderPenunjang_DetailJasa")]	public class OrderPenunjangKelompok_tableJasa_SetupModel	{		[DbColumn]		public int? Nomor { get; set; }		[DbKey]		[DbColumn]		public int? Kelompok_ID { get; set; }		[DbKey]		[DbColumn]		public string JasaID { get; set; }		public string NamaJasa { get; set; }

		[DbColumn]
		public string NamaAlias { get; set; }
    }
}