using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    public class Resep_Resep_lookup1_LookUpModel
    {
        public string KodePaket { get; set; }

        public string NamaPaket { get; set; }

        public string SectionID { get; set; }

        public DateTime LastUpdate { get; set; }

        public string Supplier_ID { get; set; }

        public bool? Racik { get; set; }

        public string Keterangan { get; set; }

        public string JenisSediaan { get; set; }
        public string Kode_Supplier { get; set; }
        public string Nama_Supplier { get; set; }
        public string SectionName { get; set; }
    }
}