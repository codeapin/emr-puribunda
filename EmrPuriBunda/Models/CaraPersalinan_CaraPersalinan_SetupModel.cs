using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mCaraPersalinan")]	public class CaraPersalinan_CaraPersalinan_SetupModel	{		[DbKey(true)]		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string CaraPersalinan { get; set; }		[DbColumn]		public bool? Aktif { get; set; }	}
}