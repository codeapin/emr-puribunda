using System;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    public class ErrorViewModel
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }

        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);

        public List<string> Errors { get; set; }
    }
}
