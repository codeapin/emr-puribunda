using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mJenisKunjungan")]	public class JenisKunjungan_JenisKunjunganModel	{		[DbKey(true)]		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string JenisKunjungan { get; set; }	}
}