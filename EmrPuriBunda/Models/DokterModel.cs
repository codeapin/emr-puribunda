﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmrPuriBunda.Models
{
    public class DokterModel
    {
        public string DokterID { get; set; }
        public string NamaDOkter {get;set;}
        public string Alamat {get;set;}
        public string NoKontak {get;set;}
        public bool? Active {get;set;}
    }
}
