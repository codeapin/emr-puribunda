using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;
using FluentValidation;
using EmrPuriBunda.Mahas.Helpers;

namespace EmrPuriBunda.Models
{
    [DbTable("trDokumenRekamMedis")]
    public class DokumenRekamMedis_DokumenRekamMedis_SetupModel
    {
        [DbKey]
        [DbColumn]
        public string NoBukti { get; set; }

        [DbColumn]
        public DateTime? TanggalDibuat { get; set; }

        [DbColumn]
        public string NoReg { get; set; }

        [DbColumn]
        public string NRM { get; set; }

        [DbColumn]
        public DateTime? TglReg { get; set; }

        [DbColumn]
        public int? JenisKerjasamaID { get; set; }

        [DbColumn]
        public string SectionID { get; set; }

        [DbColumn]
        public string UserID { get; set; }

        [DbColumn]
        public bool? RawatJalan { get; set; }

        [DbColumn]
        public bool? RawatInap { get; set; }

        [DbColumn]
        public string Catatan { get; set; }

        [DbColumn]
        public bool ArsipClaimBpjs { get; set; }

        public string NamaPasien { get; set; }

        public string JenisKelamin { get; set; }

        public string Alamat { get; set; }

        public DateTime? TglLahir { get; set; }

        public string JenisKerjasama { get; set; }

        public string JenisKerjasamaID_Text { get => JenisKerjasama; }

        public List<DokumenRekamMedis_DokumenRekamMedisDetail_SetupModel> tablePathDokumen { get; set; }
    }


    [DbTable("trDokumenRekamMedisDetail")]
    public class DokumenRekamMedis_DokumenRekamMedisDetail_SetupModel
    {
        [DbKey]
        [DbColumn]
        public string NoBukti { get; set; }

        [DbKey]
        [DbColumn]
        public int? NoUrut { get; set; }

        [DbColumn]
        public string FileName { get; set; }

        [DbColumn]
        public string FilePath { get; set; }

        [DbColumn]
        public DateTime? TanggalDibuat { get; set; }

        public string UrlDokumen { get; set; }
    }

    public class DokumenRekamMedisValidator : AbstractValidator<DokumenRekamMedis_DokumenRekamMedis_SetupModel>
    {
        public DokumenRekamMedisValidator()
        {
            RuleFor(x => x.NoBukti).HarusDiisi("No Bukti");
            RuleFor(x => x.TglReg).HarusDiisi("Tgl Pulang");
            RuleFor(x => new { x.RawatInap, x.RawatJalan }).Must(y => y.RawatJalan == true || y.RawatInap == true).WithMessage("Tipe pelayanan harus diisi");
            RuleFor(x => x.JenisKerjasamaID).HarusDiisi("Jenis Kerjasama");
        }
    }
}