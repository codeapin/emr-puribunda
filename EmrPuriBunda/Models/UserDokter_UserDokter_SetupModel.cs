using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mUserDokter")]	public class UserDokter_UserDokter_SetupModel	{		[DbKey]		[DbColumn]		public string UserId { get; set; }		[DbColumn]		public string DokterId { get; set; }		public string UserName { get; set; }		public string Nama_Supplier { get; set; }	}
}