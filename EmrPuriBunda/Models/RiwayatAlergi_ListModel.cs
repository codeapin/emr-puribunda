using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("Vw_trRiwayatAlergiPasien")]	public class RiwayatAlergi_ListModel	{		[DbColumn]		public int ID { get; set; }		[DbColumn]		public DateTime? Tanggal { get; set; }		[DbColumn]		public string NRM { get; set; }		[DbColumn]		public string RiwayatAlergi { get; set; }		[DbColumn]		public int JenisID { get; set; }		[DbColumn]		public string KeteranganAlergi { get; set; }		[DbColumn]		public string JenisID_Text { get; set; }	}
}