using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mAlasanKunjungan")]	public class AlasanKunjungan_AlasanKunjunganSetupModel	{		[DbKey(true)]		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string AlasanKunjungan { get; set; }	}
}