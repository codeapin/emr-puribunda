using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    public class InputHasilPenunjang_HasilBacaPenunjang_lookupPasien	{
        public string NoReg { get; set; }
        public DateTime TglReg { get; set; }
        public string NRM { get; set; }
        public string SectionName { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKerjasama { get; set; }
        public string StatusBayar { get; set; }
        public DateTime? TglLahir { get; set; }
        public string Alamat { get; set; }
        public string NoIdentitas { get; set; }
        public string JenisKelamin { get; set; }
        public string Umur
        {
            get
            {
                if (TglLahir == null) return "";

                var dob = TglLahir.GetValueOrDefault();

                var today = DateTime.Today;

                // Calculate the age.
                int months = today.Month - dob.Month;

                int years = today.Year - dob.Year;

                if (today.Day < dob.Day)
                {
                    months--;
                }

                if (months < 0)
                {
                    years--;
                    months += 12;
                }

                int days = (today - dob.AddMonths((years * 12) + months)).Days;

                return $"{years} tahun, {months} bulan, {days} hari";
            }
        }
    }
}