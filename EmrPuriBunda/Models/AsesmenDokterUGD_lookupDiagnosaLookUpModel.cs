using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    	public class AsesmenDokterUGD_lookupDiagnosaLookUpModel	{		public string KodeICD { get; set; }		public string SectionID { get; set; }		public string Descriptions { get; set; }		public string DiagnosaHC { get; set; }		public string IDGroupICD { get; set; }	}
}