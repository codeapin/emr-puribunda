using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("AspNetRoles")]
	public class Role_RoleSetupModel
	{
		[DbKey]
		[DbColumn]
		public string Id { get; set; }

		[DbColumn]
		public string Name { get; set; }

		[DbColumn]
		public string NormalizedName { get; set; }

		[DbColumn]
		public string ConcurrencyStamp { get; set; }
	}
}