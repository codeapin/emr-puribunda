using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mAkutKronis")]	public class AkutKronis_AkutKronis_ListModel	{		[DbKey(true)]		[DbColumn]		public byte Id { get; set; }		[DbColumn]		public string AkutKronis { get; set; }	}
}