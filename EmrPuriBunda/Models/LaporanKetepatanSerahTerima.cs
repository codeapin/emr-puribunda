using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    public class LaporanKetepatanSerahTerima_ListModel
    {
        public string NoReg { get; set; }

        public string NRM { get; set; }

        public string SectionName { get; set; }

        public string NamaDOkter { get; set; }

        public int JumlahHand { get; set; }
        public int JumlahOver { get; set; }
        public int Presentase { get; set; }
    }
}