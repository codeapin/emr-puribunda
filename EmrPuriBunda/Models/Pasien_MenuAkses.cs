﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmrPuriBunda.Models
{
    public class Pasien_MenuAkses
    {
        public string Title { get; set; }

        public string Target { get; set; }

        public string IdFormSetup { get; set; }

        public string ViewLocation { get; set; }

        public bool Allow { get; set; }

        public List<Pasien_MenuAkses> Child { get; set; }
    }
}
