using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("Vw_CatatanKeperawatan")]	public class CatatanKeperawatan_ListModel	{		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string NoReg { get; set; }		[DbColumn]		public string SectionId { get; set; }		[DbColumn]		public string NRM { get; set; }		[DbColumn]		public DateTime? Tanggal { get; set; }		[DbColumn]		public int? Id_mJenisCatatanPerawat { get; set; }		[DbColumn]		public string Profesi { get; set; }		[DbColumn]		public string Tindakan { get; set; }		[DbColumn]		public bool? PerluSerahTerima { get; set; }		[DbColumn]		public bool? PerluDoubleCheck { get; set; }		[DbColumn]		public bool? SudahSerahTerima { get; set; }		[DbColumn]		public bool? SudahDoubleCheck { get; set; }		[DbColumn]		public string SerahTerima_UserId { get; set; }		[DbColumn]		public string DoubleCheck_UserId { get; set; }		[DbColumn]		public string UserId { get; set; }		[DbColumn]		public string Id_mJenisCatatanPerawat_Text { get; set; }		[DbColumn]		public string Pembuat_Nama { get; set; }		[DbColumn]		public string DoubleCheck_Nama { get; set; }		[DbColumn]		public string Penerima_Nama { get; set; }		[DbColumn]		public string ProfesiRole { get; set; }		[DbColumn]		public bool? AdaPerubahan { get; set; }				[DbColumn]		public DateTime? LastUpdate { get; set; }

		[DbColumn]
		public DateTime? WaktuSerahTerima { get; set; }

		[DbColumn]
		public DateTime? WaktuDoubleCheck { get; set; }

        public string SectionName { get; set; }

    }
}