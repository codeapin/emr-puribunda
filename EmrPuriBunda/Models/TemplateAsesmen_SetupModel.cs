using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("mTemplateAsesmen")]	public class TemplateAsesmen_SetupModel	{		[DbKey(true)]		[DbColumn]		public int Id { get; set; }		[DbColumn]		public string Kode { get; set; }		[DbColumn]		public int? JenisID { get; set; }		[DbColumn]		public string SectionID { get; set; }		[DbColumn]		public string SpesialisasiID { get; set; }		[DbColumn]		public string DokterID { get; set; }		[DbColumn]		public string Judul { get; set; }		[DbColumn]		public string TempData { get; set; }		[DbColumn]		public bool? Active { get; set; }		public string Id_AspNetRoles { get; set; }		public string SpesialisasiID_Text { get; set; }		public string JenisID_Text { get; set; }		public string DokterID_Text { get; set; }		public string SectionID_Text { get; set; }

		public string Id_AspNetRoles_Text { get; set; }
		public List<TemplateAsesmen_TableJenis_SetupModel> TableJenis { get; set; }	}	[DbTable("mTemplateAsesmen_DetailJenis")]	public class TemplateAsesmen_TableJenis_SetupModel	{		[DbKey]		[DbColumn]		public int Id_mTemplateAsesmen { get; set; }		[DbKey]		[DbColumn]		public int Id_mJenisTemplateAsesmen { get; set; }		public string JenisTemplateAsesmen { get; set; }	}

	[DbTable("mTemplateAsesmen_DetailRoles")]	public class TemplateAsesmen_TableRoles_SetupModel	{		[DbKey]		[DbColumn]		public int Id_mTemplateAsesmen { get; set; }		[DbKey]		[DbColumn]		public string Id_AspNetRoles { get; set; }	}
}