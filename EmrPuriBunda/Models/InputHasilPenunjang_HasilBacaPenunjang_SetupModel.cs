using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using FluentValidation;
using EmrPuriBunda.Mahas.Helpers;

namespace EmrPuriBunda.Models
{
    [DbTable("trHasilBacaPenunjang")]
    public class InputHasilPenunjang_HasilBacaPenunjang_SetupModel
    {
        [DbKey]
        [DbColumn]
        public string NoBukti { get; set; }

        [DbColumn]
        public string HasilBaca { get; set; }

        [DbColumn]
        public string SectionID { get; set; }

        [DbColumn]
        public string NoReg { get; set; }

        [DbColumn]
        public bool? CriticalValue { get; set; }

        [DbColumn]
        public string VendorRujukanID { get; set; }

        [DbColumn]
        public DateTime? TanggalPeriksa { get; set; }

        [DbColumn]
        public string SectionAsalID { get; set; }

        [DbColumn]
        public string DokterID { get; set; }

        [DbColumn]
        public string PetugasID { get; set; }

        [DbColumn]
        public bool? Dibaca { get; set; }

        [DbColumn]
        public string Komentar { get; set; }

        [DbColumn]
        public string UserID { get; set; }

        [DbColumn]
        public DateTime? Tanggal { get; set; }

        [DbColumn]
        public string NRM { get; set; }

        [DbColumn]
        public string Pemeriksaan { get; set; }

        [DbColumn]
        public string NoBillPenunjang { get; set; }

        public string NamaPasien { get; set; }

        public string Alamat { get; set; }

        public string JenisKelamin { get; set; }

        public string JenisKerjasama { get; set; }

        public DateTime? TglLahir { get; set; }

        public string SectionID_Text { get; set; }

        public string VendorRujukanID_Text { get; set; }

        public string SectionAsalID_Text { get; set; }

        public string DokterID_Text { get; set; }

        public string PetugasID_Text { get; set; }

        public List<InputHasilPenunjang_HasilBacaPenunjang_tablePathDokumen_SetupModel> tablePathDokumen { get; set; }
    }

    [DbTable("trHasilBacaPenunjangDetail")]
    public class InputHasilPenunjang_HasilBacaPenunjang_tablePathDokumen_SetupModel
    {
        [DbKey]
        [DbColumn]
        public string NoBukti { get; set; }

        [DbKey]
        [DbColumn]
        public short NoUrut { get; set; }

        [JsonIgnore]
        [DbColumn]
        public string PathDokumen { get; set; }

        [DbColumn]
        public string FileName { get; set; }

        [DbColumn]
        public DateTime? Tanggal { get; set; }

        public string UrlDokumen { get; set; }
    }

    public class InputHasilPenunjangValidator: AbstractValidator<InputHasilPenunjang_HasilBacaPenunjang_SetupModel>
    {
        public InputHasilPenunjangValidator()
        {
            RuleFor(x => x.NoBukti).HarusDiisi();
            RuleFor(x => x.NRM).HarusDiisi();
            RuleFor(x => x.SectionID).HarusDiisi("Jenis Penunjang");
            RuleFor(x => x.SectionAsalID).HarusDiisi("Section Asal");
            RuleFor(x => x.DokterID).HarusDiisi("Dokter Pengirim");
            RuleFor(x => x.TanggalPeriksa).HarusDiisi("Tanggal Periksa");
            RuleFor(x => x.VendorRujukanID).HarusDiisi("Tempat Periksa");
            RuleFor(x => x.HasilBaca).HarusDiisi("Hasil Baca");
            RuleFor(x => x.Pemeriksaan).HarusDiisi();
        }
    }
}