using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;

namespace EmrPuriBunda.Models
{
    [DbTable("Vw_Obat_Detail")]
    public class ObatDetail_ObatDetail_ListModel
    {
        [DbColumn]
        public string NoBukti { get; set; }

        [DbColumn]
        public DateTime Tanggal { get; set; }

        [DbColumn]
        public string NamaDOkter { get; set; }

        [DbColumn]
        public string NoResep { get; set; }

        [DbColumn]
        public string DeskripsiObat { get; set; }

        [DbColumn]
        public string NoReg { get; set; }

        [DbColumn]
        public string Kode_Barang { get; set; }

        [DbColumn]
        public string Nama_Barang { get; set; }

        [DbColumn]
        public double JmlObat { get; set; }

        [DbColumn]
        public string Dosis { get; set; }

        public string aturan2 { get; set; }
        public string KetDosis { get; set; }
        public string Satuan { get; set; }
    }
}