using System;
using Mahas.Helpers;
using EmrPuriBunda.Models;
using System.Collections.Generic;
using FluentValidation;
using EmrPuriBunda.Mahas.Helpers;

namespace EmrPuriBunda.Models
{
    [DbTable("AsesmenDokterRI")]
    public class AsesmenDokterRI_SetupModel
    {
        [DbKey]
        [DbColumn]
        public string NoReg { get; set; }

        [DbColumn]
        public string NRM { get; set; }

        [DbColumn] 
        public DateTime? WaktuPelayanan { get; set; }

        [DbColumn]
        public byte? Id_mAlasanKunjungan { get; set; }

        [DbColumn]
        public bool? Autoanamnesis { get; set; }

        [DbColumn]
        public bool? Alloanamnesis { get; set; }

        [DbColumn]
        public string Anamnesis { get; set; }

        [DbColumn]
        public byte? Id_mHubunganPasien { get; set; }

        [DbColumn]
        public string RiwayatAlergi { get; set; }

        [DbColumn]
        public string RiwayatPenyakitDahulu { get; set; }

        [DbColumn]
        public string RiwayatPengobatan { get; set; }

        [DbColumn]
        public string PemeriksaanFisik { get; set; }

        [DbColumn]
        public string PemeriksaanPenunjang { get; set; }

        [DbColumn]
        public string SubjektifObjektif { get; set; }

        [DbColumn]
        public string DaftarMasalah { get; set; }

        [DbColumn]
        public string DiagnosaUtama { get; set; }

        [DbColumn]
        public string DiagnosaUtamaKeterangan { get; set; }

        [DbColumn]
        public string Planning_RencanaAsuhan { get; set; }

        [DbColumn]
        public string Planning_SasaranAsuhan { get; set; }

        [DbColumn]
        public int? Discharge_HariRawat { get; set; }

        [DbColumn]
        public bool? Discharge_MPP { get; set; }

        [DbColumn]
        public string Instruksi { get; set; }

        [DbColumn(isImage: true)]
        public byte[] BodyDiagram { get; set; }

        public string BodyDiagramView { get; set; }

        [DbColumn]
        public bool? IsiDenganBenar { get; set; }

        [DbColumn]
        public string UserId { get; set; }

        [DbColumn]
        public string DokterId { get; set; }

        [DbColumn]
        public string SectionId { get; set; }

        public int? IdCppt { get; set; }

        public string Id_mHubunganPasien_Text { get; set; }
        public string Id_mAlasanKunjungan_Text { get; set; }
        public string DiagnosaUtama_Text { get; set; }

        public List<AsesmenDokterRI_DiagnosaSekunder> tableDiagnosa { get; set; }
    }

    [DbTable("AsesmenDokterRI_DiagnosaSekunder")]
    public class AsesmenDokterRI_DiagnosaSekunder
    {
        [DbKey]
        [DbColumn]
        public string NoReg { get; set; }

        [DbKey]
        [DbColumn]
        public string KodeICD { get; set; }

        [DbColumn]
        public int NoUrut { get; set; }

        public string KodeICD_Text { get; set; }
    }

    public class AsesmenDokterRI_SetupModelValidator : AbstractValidator<AsesmenDokterRI_SetupModel>
    {
        public AsesmenDokterRI_SetupModelValidator()
        {
            //RuleFor(x => x.IsiDenganBenar).Must(x => x == true).WithCustomMessage("'Saya telah mengisi asesmen dengan benar' harus dicentang.");
        }
    }
}