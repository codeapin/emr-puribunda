﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmrPuriBunda.Mahas.Helpers
{
    public static class FluentValidationExtension
    {
        public const string NotEmptyMessage = "{PropertyName} harus diisi.";
        public const string GreaterThanMessage= "{PropertyName} harus lebih besar dari {ComparisonValue}";

        public static IRuleBuilderOptions<T, TProperty> WithCustomMessage<T, TProperty>(this IRuleBuilderOptions<T, TProperty> rule, string customMessage, string name = null)
        {
            if (string.IsNullOrEmpty(name))
            {
                return rule.WithMessage(customMessage);
            }
            else
            {
                return rule.WithName(name).WithMessage(customMessage);
            }
        }

        public static IRuleBuilderOptions<T, TProperty> HarusDiisi<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder, string namaProperty = null)
        {
            return ruleBuilder.NotEmpty().WithCustomMessage(NotEmptyMessage, namaProperty);
        }

        public static IRuleBuilderOptions<T, TProperty> LebihBesarDari<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder, TProperty valueToCompare, string namaProperty = null) where TProperty : IComparable<TProperty>, IComparable
        {
            if (string.IsNullOrEmpty(namaProperty))
            {
                return ruleBuilder.GreaterThan(valueToCompare);
            }
            else
            {
                return ruleBuilder.GreaterThan(valueToCompare).WithCustomMessage(GreaterThanMessage, namaProperty);
            }
        }
    }
}
