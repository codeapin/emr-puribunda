﻿namespace EmrPuriBunda.Mahas.Helpers
{
    public class AppSettings
    {
        public string BaseUrlFileManager { get; set; }

        public bool FlagDummyApp { get; set; }

        public string AppInfo { get; set; }

        public DefaultConfig DefaultConfig { get; set; }
    }

    public class DefaultConfig
    {
        public string[] SectionRadiologi { get; set; }

        public string[] SectionLab { get; set; }
        public string GroupPelayananRJ { get; set; }
        public string GroupPelayananUGD { get; set; }
        public string GroupPelayananRI { get; set; }
    }

    public class Section
    {
        public string SectionID { get; set; }

        public string SectionName { get; set; }
    }
}
