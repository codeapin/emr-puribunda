﻿using EmrPuriBunda.Areas.Identity.Data;
using EmrPuriBunda.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EmrPuriBunda.Mahas.Helpers
{
    public static class ClaimHelper
    {
        public static async Task AddClaimProfesiAsync(this UserManager<ApplicationUser> userManager, ApplicationUser user, string idProfesi, string profesi)
        {
            var claims = new List<Claim>()
            {
                new Claim("IdProfesi", idProfesi),
                new Claim("NamaDokter", profesi),
            };

            await userManager.RemoveClaimsAsync(user, claims);
            await userManager.AddClaimsAsync(user, claims);
        }

        public static async Task<User_Profesi> GetClaimProfesiAsync(this UserManager<ApplicationUser> userManager, ApplicationUser user)
        {
            var claims = await userManager.GetClaimsAsync(user);

            var idProfesi = claims.FirstOrDefault(x => x.Type == "IdProfesi");
            var profesi = claims.FirstOrDefault(x => x.Type == "NamaDokter");

            User_Profesi user_Profesi = null;

            if (idProfesi != null)
            {
                user_Profesi = new User_Profesi()
                {
                    IdProfesi = idProfesi.Value,
                    Profesi = profesi.Value
                };
            }

            return user_Profesi;
        }

        public static User_Profesi GetClaimProfesiAsync(this ClaimsPrincipal user)
        {
            var idProfesi = user.FindFirstValue("IdProfesi");
            var profesi = user.FindFirstValue("Profesi");

            User_Profesi user_Profesi = new User_Profesi()
            {
                IdProfesi = idProfesi,
                Profesi = profesi
            };

            return user_Profesi;
        }
    }
}
