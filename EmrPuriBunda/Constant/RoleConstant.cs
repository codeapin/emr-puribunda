﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmrPuriBunda.Constant;

namespace EmrPuriBunda.Constant
{
    public static class RolesConstant
    {
        public const string DokterSpesialis = "Dokter Spesialis";
        public const string Dod = "DOD";
        public const string PerawatBidan = "Perawat/Bidan";
        public const string Laboran = "Laboran";
        public const string Radiografer = "Radiografer";
        public const string Farmasi = "Farmasi";
        public const string AhliGizi = "Ahli Gizi";
        public const string RekamMedis = "Rekam Medis";
        public const string Manajemen = "Manajemen";
        public const string Admin = "Admin";
        public const string Kabid = "Kabid";
        public const string Apoteker = "Apoteker";
        public const string Terapis = "Terapis";
        public const string Psikolog = "Psikolog";
        public const string Casemic = "Casemic";

        public const string DokterSpAnak = "Dokter Sp.Anak";
        public const string DokterSpObgyn = "Dokter Sp.Obgyn";
        public const string DokterSpPD = "Dokter Sp.PD";
        public const string DokterSpAnestesi = "Dokter Sp.Anestesi";
        public const string DokterSpBedah = "Dokter Sp.Bedah";
        public const string DokterSpBedahAnak = "Dokter Sp.Bedah Anak";

        public const string CaseManager = "Case Manager";
    }
}
