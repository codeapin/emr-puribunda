﻿class iPainter {
    constructor(canvas, img, ximg, color, line, width, height) {
        this.inputtext;
        this.fontcss;
        this.canvas = document.getElementById(canvas);
        this.img = document.getElementById(img);
        this.canvas.setAttribute('width', width || this.img.width);
        this.canvas.setAttribute('height', height || this.img.height);
        this.context = this.canvas.getContext('2d');
        this.width = width;
        this.height = height;
        this.color = color || "red";
        this.line = line || 3;
        this.mousepressed = false;
        this.lastx;
        this.lasty;
        if (this.width || undefined !== undefined && this.height || undefined !== undefined)
            this.context.drawImage(ximg || this.img, 0, 0, this.width, this.height);
        else
            this.context.drawImage(ximg || this.img, 0, 0);
        var t = this;
        this.canvas.addEventListener('mousedown', function (e) {
            var inputtextvalue = t.inputtext === undefined ? '' : t.inputtext.value;
            if (inputtextvalue === '') {
                t.mousepressed = true;
                Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, false);
            } else {
                t.context.fillStyle = t.color;
                t.context.font = t.fontcss === undefined ? 'normal 20px sans-serif' : t.fontcss;
                t.context.fillText(t.inputtext.value, e.pageX - $(this).offset().left, e.pageY - $(this).offset().top);
            }
        });
        this.canvas.addEventListener('mousemove', function (e) {
            if (t.mousepressed) {
                Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, true);
            }
        });
        this.canvas.addEventListener('mouseup', function (e) {
            t.mousepressed = false;
        });
        this.canvas.addEventListener('mouseleave', function (e) {
            t.mousepressed = false;
        });
        function Draw(x, y, isdown) {
            if (isdown) {
                t.context.beginPath();
                t.context.strokeStyle = t.color;
                t.context.lineWidth = t.line;
                t.context.lineJoin = "round";
                t.context.moveTo(t.lastx, t.lasty);
                t.context.lineTo(x, y);
                t.context.closePath();
                t.context.stroke();
            }
            t.lastx = x; t.lasty = y;
        }
    }
    changecolor(color) {
        this.color = color || "#df4b26";
    }
    clear() {
        this.clickX = new Array();
        this.clickY = new Array();
        this.clickDrag = new Array();
        this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
        if (this.width || undefined !== undefined && this.height || undefined !== undefined)
            this.context.drawImage(this.img, 0, 0, this.width, this.height);
        else
            this.context.drawImage(this.img, 0, 0);
    }
    text(id, fontcss, t) {
        this.inputtext = document.getElementById(id);
        this.fontcss = fontcss;
        this.line = $(t).find('option:selected').data('line') || 3;
    }
    convert() {
        var ImageData = this.canvas.toDataURL("image/png");
        return ImageData.replace(/^data:image\/[a-z]+;base64,/, "");
    }
    loadImage(ximg) {
        if (this.width || undefined !== undefined && this.height || undefined !== undefined) {
            this.context.drawImage(ximg || this.img, 0, 0, this.width, this.height);
        }
        else {
            this.context.drawImage(ximg || this.img, 0, 0);
        }
    }
}