﻿using System.Text.RegularExpressions;

namespace EmrDokumenApi.Extensions
{
    public static class StringExtensions
    {
        public static bool IsPhoneNumber(this string number)
        {
            return Regex.Match(number, @"^\+\d{1,13}$").Success;
        }
    }
}
