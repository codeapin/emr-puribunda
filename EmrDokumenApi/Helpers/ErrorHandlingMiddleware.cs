﻿using EmrDokumenApi.Components;
using EmrDokumenApi.Components.CustomExceptions;
using Microsoft.AspNetCore.Http;
using NLog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EmrDokumenApi.Helpers
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;

        private readonly ILogger logger;

        public ErrorHandlingMiddleware(RequestDelegate next, ILogger logger)
        {
            this.next = next;
            this.logger = logger;
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                logger.Error($"Something went wrong :{ex}");

                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            var code = HttpStatusCode.InternalServerError;

            ErrorResponse error;

            if (ex is DefaultException defaultException)
            {
                error = defaultException.Error;
            }
            else
            {
                error = new ErrorResponse(ex.Message, new List<string>(), attachment: ex);
            }

            var result = JsonConvert.SerializeObject(error);

            context.Response.ContentType = "application/json";

            context.Response.StatusCode = (int)code;

            return context.Response.WriteAsync(result);
        }
    }
}
