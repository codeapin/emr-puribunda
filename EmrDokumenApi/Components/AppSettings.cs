﻿namespace EmrDokumenApi.Components
{
    public class AppSettings
    {
        public string StorageConnectionString { get; set; }

        public string AzureStorageAccountContainer { get; set; }

        public bool NotifikasiAktif { get; set; }

        public string SendGridApiKey { get; set; }

        public bool SendGridAktif { get; set; }

        public string iPaymuApiKey { get; set; }
    }
}
